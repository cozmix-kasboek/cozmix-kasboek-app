package be.cozmix.cashbook;

import android.content.Context;

import androidx.room.Room;
import androidx.test.core.app.ApplicationProvider;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.Month;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import be.cozmix.cashbook.data.database.LocalRoomDatabase;
import be.cozmix.cashbook.data.transactions.TransactionDao;
import be.cozmix.cashbook.model.Product;
import be.cozmix.cashbook.model.Transaction;
import be.cozmix.cashbook.model.TransactionProductCrossRef;
import be.cozmix.cashbook.model.TransactionWithProducts;

@RunWith(AndroidJUnit4.class)
public class DbTest {

    private LocalRoomDatabase db;

    private TransactionDao transactionDao;

    @Before
    public void createDb() {
        Context context = ApplicationProvider.getApplicationContext();
        db = Room.inMemoryDatabaseBuilder(context, LocalRoomDatabase.class).build();
        transactionDao = db.transactionDao();
    }

    @After
    public void closeDb()  {
        db.close();
    }

    @Test
    public void getTransactionWithProducts() {
        Product product1 = new Product(1, "Marsbol", 5.00, 1);
        Product product2 = new Product(2, "Marsrover", 5.00, 1);
        Product product3 = new Product(3, "Landkaart", 5.00, 1);
        LocalDateTime dateTime = LocalDateTime.of(1986, Month.APRIL, 8, 12, 30);
        Transaction transaction1 = new Transaction(1, dateTime, "Transaction 1", 25.00, 1, 1, 1, 1 );
        Transaction transaction2 = new Transaction(2, dateTime, "Transaction 2", 50.00, 1, 1, 1, 1 );
        Transaction transaction3 = new Transaction(3, dateTime, "Transaction 3", 50.00, 1, 1, 1, 1 );
        TransactionProductCrossRef transactionProductCrossRef1 = new TransactionProductCrossRef(2, 1, 10);
        TransactionProductCrossRef transactionProductCrossRef2 = new TransactionProductCrossRef(2, 2, 20);
        TransactionProductCrossRef transactionProductCrossRef3 = new TransactionProductCrossRef(2, 3, 30);

        List<Transaction> transactions = new ArrayList<>(Arrays.asList(
                transaction1,
                transaction2,
                transaction3
        ));

        List<Product> products = new ArrayList<>(Arrays.asList(
                product1,
                product2,
                product3
        ));

        List<TransactionProductCrossRef> transactionProductCrossRefs = new ArrayList<>(Arrays.asList(
                transactionProductCrossRef1,
                transactionProductCrossRef2,
                transactionProductCrossRef3
        ));


        transactionDao.insertAllTransactions(transactions);
        transactionDao.insertAllProducts(products);
        transactionDao.insertAllTransactionProductCrossReferences(transactionProductCrossRefs);

        List<TransactionWithProducts> transactionsWithProducts = db.transactionDao().getTransactionsWithProducts();
        //System.out.println(transactionsWithProducts.get(0).products.size());
        for (TransactionWithProducts twp: transactionsWithProducts){
            System.out.println(twp.transaction.getDescription());
            for (TransactionProductCrossRef p : twp.products){
                System.out.println(p.productId);
            }
        }
        //assertEquals(transactionsWithProducts.get(0).products.size(), 0);
    }
}

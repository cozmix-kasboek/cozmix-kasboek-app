package be.cozmix.cashbook;

import androidx.test.ext.junit.runners.AndroidJUnit4;

import org.junit.Test;
import org.junit.runner.RunWith;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

import static junit.framework.TestCase.fail;

@RunWith(AndroidJUnit4.class)
public class DateTimeFormatterTest {

    private final DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("d/M/yyyy");
    private final DateTimeFormatter timeFormatter = DateTimeFormatter.ofPattern("H:mm");

    @Test
    public void testDateFormatter() {
        String date = "1/1/2020";
        try {
            System.out.println(dateFormatter.parse(date));
            System.out.println(LocalDate.parse(date, dateFormatter));
        } catch (DateTimeParseException exc) {
            fail();
        }
    }

    @Test
    public void testTimeFormatter() {
        String time = "9:00";
        try {
            System.out.println(LocalTime.parse(time, timeFormatter));
        } catch (DateTimeParseException exc) {
            fail();
        }
    }

}

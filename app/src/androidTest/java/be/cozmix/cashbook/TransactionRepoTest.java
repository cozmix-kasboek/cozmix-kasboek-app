package be.cozmix.cashbook;

import android.content.Context;

import androidx.room.Room;
import androidx.test.core.app.ApplicationProvider;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.Month;

import be.cozmix.cashbook.data.database.LocalRoomDatabase;
import be.cozmix.cashbook.data.transactions.TransactionDao;
import be.cozmix.cashbook.model.Product;
import be.cozmix.cashbook.model.Transaction;

import static org.junit.Assert.assertEquals;

@RunWith(AndroidJUnit4.class)
public class TransactionRepoTest {

    private TransactionDao transactionDao;
    private LocalRoomDatabase db;

    @Before
    public void createDb() {
        Context context = ApplicationProvider.getApplicationContext();
        db = Room.inMemoryDatabaseBuilder(context, LocalRoomDatabase.class).build();
        transactionDao= db.transactionDao();
    }

    @After
    public void closeDb() throws IOException {
        db.close();
    }

    @Test
    public void writeProductAndReadInList() throws Exception {

        LocalDateTime dateTime = LocalDateTime.of(1986, Month.APRIL, 8, 12, 30);


        Transaction transaction = new Transaction(2, dateTime, "Trans", 25.00, 1, 1, 1, 1 );
        Product product = new Product(1, "mars", 1);
        Product product1 = new Product(2, "Pluto", 2);
        Product product2 = new Product(3, "Venus", 3);

        transactionDao.insert(transaction);
        assertEquals(transactionDao.getTransactionById(2).getUser(), 1);
    }
}

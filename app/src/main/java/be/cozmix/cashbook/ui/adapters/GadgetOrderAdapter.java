package be.cozmix.cashbook.ui.adapters;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import be.cozmix.cashbook.R;
import be.cozmix.cashbook.data.gadgetOrders.GadgetOrderRepository;
import be.cozmix.cashbook.data.gadgetOrders.InMemoryGadgetOrderRepository;
import be.cozmix.cashbook.model.GadgetOrder;

public class GadgetOrderAdapter extends RecyclerView.Adapter<GadgetOrderAdapter.GadgetOrderViewHolder> {

    private static final String TAG = "MyActivity";
    public Context mContext;
    public List<GadgetOrder> mGadgetOrders;
    private GadgetOrderRepository mRepository;
    private TextView mPrice;

    public GadgetOrderAdapter(Context context, List<GadgetOrder> mGadgetOrders) {
        this.mGadgetOrders = mGadgetOrders;
        this.mContext = context;
        mRepository = InMemoryGadgetOrderRepository.getInstance();
        mPrice = (TextView) ((Activity) context).findViewById(R.id.total_price);
    }
    @NonNull
    @Override
    public GadgetOrderViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater mInflater = LayoutInflater.from(mContext);
        View mGadgetOrderView =
                mInflater.inflate(R.layout.item_gadget, parent, false);
        return new GadgetOrderViewHolder(mGadgetOrderView);
    }

    @Override
    public void onBindViewHolder(@NonNull GadgetOrderAdapter.GadgetOrderViewHolder holder, int position) {
        final GadgetOrder mCurrent = mGadgetOrders.get(position);
        Log.v(TAG, "index=" + mCurrent.getQuantity());
        holder.gadgetName.setText(mCurrent.getProductName());
        holder.gadgetQuantity.setText(String.valueOf(mCurrent.getQuantity()));

        holder.minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCurrent.decreaseQuantity();
                String priceText = "TOTAAL "+InMemoryGadgetOrderRepository.getInstance().calculateTotalPrice()+" EURO";
                mPrice.setText(priceText);
                notifyDataSetChanged();
            }
        });

        holder.plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCurrent.increaseQuantity();
                String priceText = "TOTAAL "+InMemoryGadgetOrderRepository.getInstance().calculateTotalPrice()+" EURO";
                mPrice.setText(priceText);
                notifyDataSetChanged();
            }
        });

        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mRepository.deleteItem(mCurrent);
                String priceText = "TOTAAL "+InMemoryGadgetOrderRepository.getInstance().calculateTotalPrice()+" EURO";
                mPrice.setText(priceText);
                notifyDataSetChanged();
            }
        });



    }

    @Override
    public int getItemCount() {
        return mGadgetOrders.size();
    }

    class GadgetOrderViewHolder extends RecyclerView.ViewHolder {

        private TextView gadgetName;
        private TextView gadgetQuantity;
        private Button minus;
        private Button plus;
        private TextView delete;

        GadgetOrderViewHolder(@NonNull View itemView) {
            super(itemView);
            gadgetName = itemView.findViewById(R.id.gadget_name);
            gadgetQuantity = itemView.findViewById(R.id.quantity);
            minus = itemView.findViewById(R.id.button_minus);
            plus = itemView.findViewById(R.id.button_plus);
            delete = itemView.findViewById(R.id.delete_gadget);

        }
    }
}

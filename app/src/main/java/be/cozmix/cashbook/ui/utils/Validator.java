package be.cozmix.cashbook.ui.utils;

public interface Validator {

    boolean validate(String input);
    String getErrorMessage();

}

package be.cozmix.cashbook.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

import be.cozmix.cashbook.R;

public class OtherTransactionsActivity extends AppCompatActivity {

    private Button memberships;
    private Button expenses;
    private Button diverse;
    private Button bank;

    private Button cancel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_other_transactions);
        setLayout();
        setButtons();
    }

    private void setLayout() {
        memberships = findViewById(R.id.btn_memberships);
        expenses = findViewById(R.id.btn_expenses);
        diverse = findViewById(R.id.btn_diverse);
        bank = findViewById(R.id.btn_bank);
        cancel = findViewById(R.id.cancel);
    }

    private void setButtons() {
        memberships.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplication(), MembershipActivity.class));
            }
        });
        expenses.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplication(), ExpensesActivity.class));
            }
        });
        diverse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplication(), DiverseActivity.class));
            }
        });
        bank.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplication(), BankActivity.class));
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplication(), HomeActivity.class));
                finish();
            }
        });
    }

}

package be.cozmix.cashbook.ui.activities;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import be.cozmix.cashbook.R;
import be.cozmix.cashbook.model.Product;
import be.cozmix.cashbook.ui.adapters.TicketAdapter;
import be.cozmix.cashbook.ui.viewmodels.TicketsViewModel;

public class TicketsActivity extends AppCompatActivity {

    private RecyclerView mRecyclerView;
    private TicketAdapter mAdapter;
    private TicketsViewModel mViewModel;

    private List<Product> mTickets = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tickets);
        initViewModel();
        initRecyclerView();
    }

    private void initViewModel() {
        Observer<List<Product>> ticketsObserver = new Observer<List<Product>>() {
            @Override
            public void onChanged(final List<Product> tickets) {
                mTickets.clear();
                mTickets.addAll(tickets);
                notifyAdapter(tickets);
            }
        };

        mViewModel = ViewModelProviders.of(this).get(TicketsViewModel.class);
        mViewModel.getGadgets().observe(this, ticketsObserver);
    }

    private void notifyAdapter(List<Product> tickets) {
        if (mAdapter == null) {
            mAdapter = new TicketAdapter(getApplicationContext(), tickets);
            mRecyclerView.setAdapter(mAdapter);
        } else {
            mAdapter.notifyDataSetChanged();
        }
    }

    private void initRecyclerView() {
        mRecyclerView = findViewById(R.id.recyclerview_tickets);

        mAdapter = new TicketAdapter(this, mTickets);
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

}

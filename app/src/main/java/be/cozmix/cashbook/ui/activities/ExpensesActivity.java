package be.cozmix.cashbook.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import java.time.LocalDateTime;
import java.util.List;

import be.cozmix.cashbook.R;
import be.cozmix.cashbook.data.database.RoomCashBookRepository;
import be.cozmix.cashbook.data.utils.RoomCallback;
import be.cozmix.cashbook.model.Product;
import be.cozmix.cashbook.model.Transaction;
import be.cozmix.cashbook.model.TransactionProductCrossRef;
import be.cozmix.cashbook.ui.adapters.ExpenseAdapter;
import be.cozmix.cashbook.ui.viewmodels.ExpenseViewModel;

public class ExpensesActivity extends AppCompatActivity {

    private LinearLayout form;

    private Spinner type;
    private EditText amount;
    private EditText remark;

    private TextView cancel;
    private TextView confirm;

    private RoomCashBookRepository cashBookRepository;

    private ExpenseViewModel expenseViewModel;

    private ExpenseAdapter adapter;
    private final be.cozmix.cashbook.utils.UserManager userManager = be.cozmix.cashbook.utils.UserManager.getInstance();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_expenses);
        cashBookRepository = RoomCashBookRepository.getInstance(getApplicationContext());
        setLayout();

        expenseViewModel = ViewModelProviders.of(this).get(ExpenseViewModel.class);
        expenseViewModel.getExpenses().observe(this, new Observer<List<Product>>() {
            @Override
            public void onChanged(@Nullable final List<Product> expenses) {
                adapter = new ExpenseAdapter(getApplicationContext(), android.R.layout.simple_spinner_dropdown_item, expenses);
                type = (Spinner) findViewById(R.id.spinner_types);
                type.setAdapter(adapter);

            }
        });
    }

    private void setLayout() {
        form = findViewById(R.id.form);
        type = findViewById(R.id.spinner_types);
        amount = findViewById(R.id.txt_expenses_amount);
        remark = findViewById(R.id.txt_expenses_remark);
        cancel = findViewById(R.id.cancel);
        confirm = findViewById(R.id.confirm);

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println(remark.getText().toString());

                if (checkIfExpenseAmountFilledOut()) {
                    saveExpense();
                }
            }
        });
    }


    public void saveExpense() {
        LocalDateTime dateTime = LocalDateTime.now();
        String comment = remark.getText().toString();
        double price = Double.valueOf(amount.getText().toString());
        int userId = userManager.getUser().getId();
        Product p = (Product) type.getSelectedItem();
        int selectedExpenseId = p.getProductId();


        double negativePrice = price * -1;
        Transaction transaction = new Transaction(0, dateTime, comment, negativePrice, userId, 6, "", 0);

        cashBookRepository.getTransactionEntityManager().addAndGetId(transaction, new RoomCallback() {
            @Override
            public void onSuccess(int id) {
                System.out.println("Success");
                TransactionProductCrossRef transactionProductCrossRef = new TransactionProductCrossRef(id, selectedExpenseId, 1);
                System.out.println(transactionProductCrossRef);
                cashBookRepository.getTransactionEntityManager().insertTransactionProductCrossReference(transactionProductCrossRef);
                Intent intent = new Intent(getApplication(), ConfirmationActivity.class);
                startActivity(intent);
                finish();
            }
        });


    }

    public boolean checkIfExpenseAmountFilledOut() {
        if (TextUtils.isEmpty(amount.getText().toString())) {
            amount.setError("Mag niet leeg zijn");
            showToastMessage("Gelieve het bedrag van de onkost in te vullen");
            return false;
        }
        return true;
    }


    public void showToastMessage(String message) {
        int duration = Toast.LENGTH_LONG;
        Toast toast = Toast.makeText(getApplicationContext(), message, duration);
        toast.show();
    }


}

package be.cozmix.cashbook.ui.utils.events;

import java.util.Locale;

import be.cozmix.cashbook.ui.utils.Validator;

public class EventNameValidator implements Validator {

    private static final int MINIMUM_NAME_LENGTH = 4;
    private static final int MAXIMUM_NAME_LENGTH = 128;

    private static final String ERROR_NAME_REQUIRED = "Omschrijving is vereist.";
    private static final String ERROR_NAME_TOO_SHORT = String.format(Locale.US,
            "Omschrijving moet minimum %d letters lang zijn.", MINIMUM_NAME_LENGTH);
    private static final String ERROR_NAME_TOO_LONG = String.format(Locale.US,
            "Omschrijving mag maximum %d letters lang zijn.", MAXIMUM_NAME_LENGTH);

    private String error;

    @Override
    public boolean validate(String input) {
        if (input == null)
            return false;

        int length = input.trim().length();

        if (length <= 0) {
            error = ERROR_NAME_REQUIRED;
        } else if (length < MINIMUM_NAME_LENGTH) {
            error = ERROR_NAME_TOO_SHORT;
        } else if (length > MAXIMUM_NAME_LENGTH) {
            error = ERROR_NAME_TOO_LONG;
        } else {
            error = null;
            return true;
        }

        return false;
    }

    @Override
    public String getErrorMessage() {
        return error;
    }

}

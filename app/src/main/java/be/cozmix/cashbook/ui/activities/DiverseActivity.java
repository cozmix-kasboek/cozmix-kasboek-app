package be.cozmix.cashbook.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import java.time.LocalDateTime;
import java.util.List;

import be.cozmix.cashbook.R;
import be.cozmix.cashbook.data.database.RoomCashBookRepository;
import be.cozmix.cashbook.data.transactions.ITransactionEntityManager;
import be.cozmix.cashbook.data.utils.RoomCallback;
import be.cozmix.cashbook.model.Product;
import be.cozmix.cashbook.model.Transaction;
import be.cozmix.cashbook.model.TransactionProductCrossRef;
import be.cozmix.cashbook.ui.adapters.DiverseAdapter;
import be.cozmix.cashbook.ui.viewmodels.DiverseViewModel;

public class DiverseActivity extends AppCompatActivity {

    private static final String TAG = DiverseActivity.class.getSimpleName();
    private final String message = "Diverse betaling in verband met %s (Opmerking: %s).";

    private Spinner type;
    private EditText name;
    private EditText amount;
    private EditText remark;

    private RadioGroup radioGroup;

    private TextView cancel;
    private TextView confirm;

    private DiverseViewModel diverseViewModel;
    private DiverseAdapter adapter;

    private ITransactionEntityManager transactionManager;

    private final be.cozmix.cashbook.utils.UserManager userManager = be.cozmix.cashbook.utils.UserManager.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_diverse);
        transactionManager = RoomCashBookRepository.getInstance(this).getTransactionEntityManager();
        setLayout();

        diverseViewModel = ViewModelProviders.of(this).get(DiverseViewModel.class);
        diverseViewModel.getExpenses().observe(this, new Observer<List<Product>>() {
            @Override
            public void onChanged(List<Product> products) {
                adapter = new DiverseAdapter(getApplicationContext(), android.R.layout.simple_spinner_dropdown_item, products);
                type = (Spinner) findViewById(R.id.spinner_types_diverse);
                type.setAdapter(adapter);
            }
        });
    }

    private void setLayout() {
        amount = findViewById(R.id.txt_diverse_amount);
        remark = findViewById(R.id.txt_diverse_remark);
        radioGroup = findViewById(R.id.radio_payment);
        cancel = findViewById(R.id.cancel);
        confirm = findViewById(R.id.confirm);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int selectedId = radioGroup.getCheckedRadioButtonId();

                if (checkIfExpenseAmountFilledOut() && checkIfPaymentMethodSelected(selectedId)) {
                    saveDiverse();
                }
                else {
                    showToastMessage("Gelieve bedrag in te vullen en betaalmethode te selecteren");
                }
            }
        });
    }

    public void saveDiverse(){
        LocalDateTime dateTime = LocalDateTime.now();
        String comment = remark.getText().toString();
        double price = Double.valueOf(amount.getText().toString());
        int userId = userManager.getUser().getId();
        Product p = (Product) type.getSelectedItem();
        int selectedExpenseId = p.getProductId();

        Transaction transaction = new Transaction(0, dateTime, comment, price, userId, 4, "", 0);

        transactionManager.addAndGetId(transaction, new RoomCallback() {
            @Override
            public void onSuccess(int id) {
                System.out.println("Success");
                TransactionProductCrossRef transactionProductCrossRef = new TransactionProductCrossRef(id, selectedExpenseId, 1);
                System.out.println(transactionProductCrossRef);
                transactionManager.insertTransactionProductCrossReference(transactionProductCrossRef);
                Intent intent = new Intent(getApplication(), ConfirmationActivity.class);
                startActivity(intent);
                finish();

            }
        });
    }

    public boolean checkIfExpenseAmountFilledOut() {
        if (TextUtils.isEmpty(amount.getText().toString())) {
            amount.setError("Mag niet leeg zijn");
            return false;
        }
        return true;
    }

    public void showToastMessage(String message) {
        int duration = Toast.LENGTH_LONG;
        Toast toast = Toast.makeText(getApplicationContext(), message, duration);
        toast.show();
    }

    public boolean checkIfPaymentMethodSelected(int id){
        return id != -1;
    }

}

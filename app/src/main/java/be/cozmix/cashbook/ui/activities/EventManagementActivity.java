package be.cozmix.cashbook.ui.activities;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import be.cozmix.cashbook.R;
import be.cozmix.cashbook.model.Event;
import be.cozmix.cashbook.ui.viewmodels.EventsViewModel;

public class EventManagementActivity extends AppCompatActivity {

    private TextView currentDate;
    private TextView noEvents;
    private TableLayout table;
    private TextView addEvent;
    private Button cancel;

    private EventsViewModel mViewModel;
    private List<Event> mEvents = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_management);

        setLayout();
        showCurrentDateOnScreen();
        initViewModel();
    }

    private void setLayout() {


        this.currentDate = findViewById(R.id.txt_event_management_date);
        this.noEvents = findViewById(R.id.no_events);
        this.table = findViewById(R.id.events_table);
        this.addEvent = findViewById(R.id.btn_add_event);
        this.cancel = findViewById(R.id.cancel);
        addEvent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplication(), EventAddActivity.class));
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplication(), HomeActivity.class));
                finish();
            }
        });
    }

    private void initViewModel() {
        Observer<List<Event>> eventsObserver = new Observer<List<Event>>() {
            @Override
            public void onChanged(List<Event> events) {
                mEvents.clear();
                table.removeAllViews();
                mEvents.addAll(events);
                render(events);
            }
        };

        mViewModel = ViewModelProviders.of(this).get(EventsViewModel.class);
        mViewModel.getEvents(LocalDate.now()).observe(this, eventsObserver);
    }

    private void render(List<Event> events) {
        if (events == null || events.size() <= 0) {
            noEvents.setVisibility(View.VISIBLE);
            table.setVisibility(View.GONE);
        } else {
            for (int i = 0; i < events.size(); i++) {
                Event event = events.get(i);

                TableRow row = new TableRow(this);
                TableRow.LayoutParams p = new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT);
                row.setLayoutParams(p);

                TextView name = new TextView(this);
                TextView time = new TextView(this);
                TextView edit = new TextView(this);

                name.setText(event.getName());
                time.setText(event.getTime().toString());
                edit.setText(R.string.alter_event_data);
                edit.setTypeface(null, Typeface.BOLD);
                edit.setPaintFlags(edit.getPaintFlags()| Paint.UNDERLINE_TEXT_FLAG);
                edit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(getApplication(), EventEditActivity.class);
                        intent.putExtra("id", event.getId());
                        startActivity(intent);
                    }
                });

                TextView[] textViews = new TextView[]{name, time, edit};
                for (TextView textView : textViews) {
                    textView.setGravity(Gravity.CENTER);
                    textView.setPadding(8,8,8,8);
                    textView.setTextColor(Color.BLACK);
                    textView.setTextSize(18);
                    row.addView(textView);
                }

                table.addView(row, i);
            }
            noEvents.setVisibility(View.GONE);
            table.setVisibility(View.VISIBLE);
        }
    }

    private void showCurrentDateOnScreen(){
        ZoneId zoneId = ZoneId.of("Europe/Brussels");

        Instant instant = Instant.now();

        ZonedDateTime zDateTime = instant.atZone(zoneId);

        currentDate.setText(zDateTime.format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
    }
}

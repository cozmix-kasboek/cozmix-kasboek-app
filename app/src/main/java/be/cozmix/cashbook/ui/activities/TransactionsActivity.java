package be.cozmix.cashbook.ui.activities;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import be.cozmix.cashbook.R;
import be.cozmix.cashbook.model.Transaction;
import be.cozmix.cashbook.ui.viewmodels.TransactionsViewModel;
import be.cozmix.cashbook.utils.TransactionTypeMapper;

public class TransactionsActivity extends AppCompatActivity {

    private TextView dateText;
    private TableLayout table;
    private TextView home;

    private LocalDate date = LocalDate.now();
    private DateTimeFormatter renderFormatter = DateTimeFormatter.ofPattern("d/M/yyyy");

    private TransactionsViewModel mViewModel;
    private List<Transaction> mTransactions = new ArrayList<>();
    private TransactionTypeMapper transactionTypeMapper = new TransactionTypeMapper();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transactions);
        setLayout();
        initViewModel();
    }

    private void setLayout() {
        dateText = findViewById(R.id.txt_current_date);
        table = findViewById(R.id.transactions_table);
        home = findViewById(R.id.btn_back_to_homepage);
        dateText.setText(renderFormatter.format(date));
        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplication(), HomeActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }

    private void initViewModel() {
        Observer<List<Transaction>> transactionsObserver = new Observer<List<Transaction>>() {
            @Override
            public void onChanged(List<Transaction> transactions) {
                mTransactions.clear();
                clearTable();
                mTransactions.addAll(transactions);
                render(transactions);
            }
        };

        mViewModel = ViewModelProviders.of(this).get(TransactionsViewModel.class);
        mViewModel.getTransactions(date).observe(this, transactionsObserver);
    }

    private void render(List<Transaction> transactions) {
        if (transactions != null && transactions.size() > 0) {
            for (int i = 0; i < transactions.size(); i++) {
                Transaction transaction = transactions.get(i);

                TableRow row = new TableRow(this);
                TableRow.LayoutParams p = new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT);
                row.setLayoutParams(p);

                TextView time, type, details, amount, action;
                time = new TextView(this);
                type = new TextView(this);
                details = new TextView(this);
                amount = new TextView(this);
                action = new TextView(this);


                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("H:mm");
                time.setText(formatter.format(transaction.getDate()));
                type.setText(transactionTypeMapper.getMyMap().get((transaction.getType())));
                String detailsString = transaction.getDescription();
                if (detailsString.length() > 30){
                    detailsString = detailsString.substring(0, Math.min(detailsString.length(), 30)) + "...";
                }
                details.setText(detailsString);
                amount.setText(String.format(Locale.US, "%.2f EUR", transaction.getTransactionAmount()));
                action.setText(R.string.undo);
                action.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mViewModel.delete(transaction);
                    }
                });

                TextView[] textViews = new TextView[]{time, type, details, amount, action};
                for (TextView textView : textViews) {
                    textView.setGravity(Gravity.START);
                    textView.setPadding(8,8,8,8);
                    textView.setTextColor(Color.BLACK);
                    textView.setTextSize(18);
                    row.addView(textView);
                }
                action.setTypeface(null, Typeface.BOLD);

                table.addView(row, i+1);
            }
        }
    }

    private void clearTable() {
        while (table.getChildCount() > 1)
            table.removeView(table.getChildAt(table.getChildCount() - 1));
    }

}

package be.cozmix.cashbook.ui.viewmodels;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

import be.cozmix.cashbook.data.ticketOrders.InMemoryTicketOrderRepository;
import be.cozmix.cashbook.data.ticketOrders.TicketOrderRepository;
import be.cozmix.cashbook.model.TicketOrder;

public class TicketsOrderViewModel extends AndroidViewModel {

    private TicketOrderRepository mRepository;
    private LiveData<List<TicketOrder>> mTicketOrders;

    public TicketsOrderViewModel(@NonNull Application application) {
        super(application);
        this.mRepository = InMemoryTicketOrderRepository.getInstance();
        this.mTicketOrders = mRepository.getAll();
    }

    public LiveData<List<TicketOrder>> getTicketOrders() { return mTicketOrders; }

}

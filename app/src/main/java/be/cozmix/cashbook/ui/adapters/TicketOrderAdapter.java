package be.cozmix.cashbook.ui.adapters;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import be.cozmix.cashbook.R;
import be.cozmix.cashbook.data.ticketOrders.InMemoryTicketOrderRepository;
import be.cozmix.cashbook.data.ticketOrders.TicketOrderRepository;
import be.cozmix.cashbook.model.TicketOrder;

public class TicketOrderAdapter extends RecyclerView.Adapter<TicketOrderAdapter.TicketOrderViewHolder> {

    private static final String TAG = "MyActivity";
    public Context mContext;
    public List<TicketOrder> mTicketOrders;
    private TicketOrderRepository mRepository;
    private TextView mPrice;

    public TicketOrderAdapter(Context context, List<TicketOrder> mTicketOrders){
        this.mTicketOrders = mTicketOrders;
        this.mContext = context;
        mRepository = InMemoryTicketOrderRepository.getInstance();
        mPrice = (TextView) ((Activity) context).findViewById(R.id.total_price);
    }

    @NonNull
    @Override
    public TicketOrderAdapter.TicketOrderViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater mInflater = LayoutInflater.from(mContext);
        View mTicketOrderView = mInflater.inflate(R.layout.item_ticket, parent, false);
        return new TicketOrderViewHolder(mTicketOrderView);
    }

    @Override
    public void onBindViewHolder(@NonNull TicketOrderAdapter.TicketOrderViewHolder holder, int position) {
        final TicketOrder mCurrent = mTicketOrders.get(position);
        Log.v(TAG, "index=" + mCurrent.getQuantity());
        holder.ticketName.setText(mCurrent.getProductName());
        holder.ticketQuantity.setText(String.valueOf(mCurrent.getQuantity()));

        holder.minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCurrent.decreaseQuantity();
                String priceText = "TOTAAL "+ InMemoryTicketOrderRepository.getInstance().calculateTotalPrice()+" EURO";
                mPrice.setText(priceText);
                notifyDataSetChanged();
            }
        });

        holder.plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCurrent.increaseQuantity();
                String priceText = "TOTAAL "+InMemoryTicketOrderRepository.getInstance().calculateTotalPrice()+" EURO";
                mPrice.setText(priceText);
                notifyDataSetChanged();
            }
        });

        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mRepository.deleteItem(mCurrent);
                String priceText = "TOTAAL "+InMemoryTicketOrderRepository.getInstance().calculateTotalPrice()+" EURO";
                mPrice.setText(priceText);
                mPrice.setText(priceText);
                notifyDataSetChanged();
            }
        });



    }

    @Override
    public int getItemCount() {
        return mTicketOrders.size();
    }


    class TicketOrderViewHolder extends RecyclerView.ViewHolder {

        private TextView ticketName;
        private TextView ticketQuantity;
        private Button minus;
        private Button plus;
        private TextView delete;

        TicketOrderViewHolder(@NonNull View itemView) {
            super(itemView);
            ticketName = itemView.findViewById(R.id.ticket_name);
            ticketQuantity = itemView.findViewById(R.id.quantity);
            minus = itemView.findViewById(R.id.button_minus);
            plus = itemView.findViewById(R.id.button_plus);
            delete = itemView.findViewById(R.id.delete_ticket);

        }
    }

}

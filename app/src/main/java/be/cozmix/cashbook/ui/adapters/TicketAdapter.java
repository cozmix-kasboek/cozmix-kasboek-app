package be.cozmix.cashbook.ui.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import be.cozmix.cashbook.R;
import be.cozmix.cashbook.data.ticketOrders.InMemoryTicketOrderRepository;
import be.cozmix.cashbook.data.ticketOrders.TicketOrderRepository;
import be.cozmix.cashbook.model.Product;
import be.cozmix.cashbook.ui.activities.TicketsPaymentActivity;

public class TicketAdapter extends RecyclerView.Adapter<TicketAdapter.TicketViewHolder> {

    private Context mContext;
    private List<Product> mTickets;
    private TicketOrderRepository mRepository;

    public TicketAdapter(Context context, List<Product> mTickets) {
        this.mTickets = mTickets;
        this.mContext = context;
        mRepository = InMemoryTicketOrderRepository.getInstance();
    }

    @NonNull
    @Override
    public TicketAdapter.TicketViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater mInflater = LayoutInflater.from(mContext);
        View mTicketView = mInflater.inflate(R.layout.card_ticket, parent, false);
        return new TicketViewHolder(mTicketView);
    }

    @Override
    public void onBindViewHolder(@NonNull TicketAdapter.TicketViewHolder holder, int position) {
        final Product mCurrent = mTickets.get(position);
        String result = "€ "+String.format("%.2f", mCurrent.getPrice());
        holder.ticketName.setText(mCurrent.getName());
        holder.ticketPrice.setText(result);
        holder.ticketCard.setOnClickListener(v -> {
            Intent intent = new Intent(mContext, TicketsPaymentActivity.class);
            mRepository.addItem(mCurrent.getProductId(), mCurrent.getName(), mCurrent.getPrice());
            mContext.startActivity(intent);
        });
    }

    @Override
    public int getItemCount() {
        return mTickets.size();
    }

    class TicketViewHolder extends RecyclerView.ViewHolder {

        private CardView ticketCard;
        private TextView ticketName;
        private TextView ticketPrice;
        public TicketViewHolder(@NonNull View itemView) {
            super(itemView);
            ticketCard = itemView.findViewById(R.id.ticket_card);
            ticketName = itemView.findViewById(R.id.ticket_name);
            ticketPrice = itemView.findViewById(R.id.ticket_price);
        }
    }
}

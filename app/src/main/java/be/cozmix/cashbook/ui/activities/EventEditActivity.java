package be.cozmix.cashbook.ui.activities;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

import be.cozmix.cashbook.R;
import be.cozmix.cashbook.model.Event;
import be.cozmix.cashbook.ui.utils.events.EventEditFormState;
import be.cozmix.cashbook.ui.viewmodels.EventEditViewModel;
import be.cozmix.cashbook.utils.ViewModelFactory;

public class EventEditActivity extends AppCompatActivity {

    private TextView subtitle;

    private EditText name;
    private EditText time;

    private String nameString;
    private String timeString;

    private TextView error;
    private Button delete;

    private TextView cancel;
    private TextView confirm;

    private EventEditViewModel mViewModel;
    private Event mEvent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_edit);

        Bundle extras = getIntent().getExtras();
        if (extras == null) {
            Toast.makeText(getApplicationContext(), "Failed.", Toast.LENGTH_SHORT).show();
            finish();
        } else {
            String id = extras.getString("id");
            setLayout();
            initViewModel(id);
            initTextWatcher(id);
        }
    }

    private void setLayout() {
        this.subtitle = findViewById(R.id.subtitle);
        this.name = findViewById(R.id.txt_event_description);
        this.time = findViewById(R.id.txt_event_time);
        this.error = findViewById(R.id.txt_event_error);
        this.delete = findViewById(R.id.btn_delete_event);
        this.cancel = findViewById(R.id.cancel);
        this.confirm = findViewById(R.id.confirm);
        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mViewModel.delete(mEvent);
                finish();
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void initViewModel(String id) {
        Observer<EventEditFormState> eventEditFormStateObserver = new Observer<EventEditFormState>() {
            @Override
            public void onChanged(EventEditFormState eventEditFormState) {
                if (eventEditFormState == null) {
                    return;
                }
                enableConfirm(eventEditFormState.isDataValid());
                if (!eventEditFormState.isDataValid()) {
                    if (eventEditFormState.hasNameError()) {
                        error.setText(eventEditFormState.getNameError());
                    }
                    if (eventEditFormState.hasTimeError()) {
                        error.setText(eventEditFormState.getTimeError());
                    }
                    error.setVisibility(View.VISIBLE);
                } else {
                    error.setVisibility(View.INVISIBLE);
                }
            }
        };

        Observer<Event> eventObserver = new Observer<Event>() {
            @Override
            public void onChanged(Event event) {
                mEvent = event;
                render(event);
            }
        };

        mViewModel = ViewModelProviders.of(this,
                new ViewModelFactory(getApplication(), id)).get(EventEditViewModel.class);
        mViewModel.getEvent().observe(this, eventObserver);
        mViewModel.getEventFormState().observe(this, eventEditFormStateObserver);
    }

    private void initTextWatcher(String id) {
        TextWatcher afterTextChangedListener = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // Android recommends you leave this method empty.
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // Android recommends you leave this method empty.
            }

            @Override
            public void afterTextChanged(Editable s) {
                nameString = name.getText().toString();
                timeString = time.getText().toString();

                mViewModel.isDataValid(nameString, timeString);
            }
        };

        TextView.OnEditorActionListener actionListener = new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE &&
                        mViewModel.isDataValid(nameString, timeString)) {
                    update(id);
                    finish();
                }
                return false;
            }
        };

        name.addTextChangedListener(afterTextChangedListener);
        time.addTextChangedListener(afterTextChangedListener);
        name.setOnEditorActionListener(actionListener);
        time.setOnEditorActionListener(actionListener);
        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mViewModel.isDataValid(nameString, timeString)) {
                    update(id);
                    finish();
                }
            }
        });
    }

    private void render(Event event) {
        if (event == null)
            return;
        subtitle.setText(String.format(Locale.US,
                "Je wijzigt momenteel de gebeurtenis \n%s met aanvang om %s \nop %s.",
                event.getName(), event.getTime(), event.getDate()));
        name.setText(event.getName());
        time.setText(event.getTime().toString());
    }

    private void update(String id) {
        DateTimeFormatter timeFormatter = DateTimeFormatter.ofPattern("H:mm");
        LocalTime localTime = LocalTime.parse(timeString, timeFormatter);
        Event event = new Event(id, nameString, mEvent.getDate(), localTime);
        mViewModel.update(event);
    }

    private void enableConfirm(boolean enable) {
        int color = R.color.colorDisabled;
        int drawable = R.drawable.ic_check_gray;
        if (enable) {
            color = R.color.colorBlack;
            drawable = R.drawable.ic_check_black;
        }

        confirm.setEnabled(enable);
        confirm.setTextColor(getResources().getColor(color, getTheme()));
        confirm.setCompoundDrawables(null,
                getResources().getDrawable(drawable, getTheme()), null, null);
    }

}

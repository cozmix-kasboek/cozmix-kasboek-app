package be.cozmix.cashbook.ui.viewmodels;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

import be.cozmix.cashbook.data.database.RoomCashBookRepository;
import be.cozmix.cashbook.model.Product;

public class TicketsViewModel extends AndroidViewModel {
    private LiveData<List<Product>> mTickets;

    public TicketsViewModel(Application application) {
        super(application);
        mTickets = RoomCashBookRepository.getInstance(application).getProductEntityManager().getTickets();
    }

    public LiveData<List<Product>> getGadgets() {
        return mTickets;
    }
}

package be.cozmix.cashbook.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import be.cozmix.cashbook.R;
import be.cozmix.cashbook.data.database.RoomCashBookRepository;
import be.cozmix.cashbook.data.gadgetOrders.InMemoryGadgetOrderRepository;
import be.cozmix.cashbook.data.paymentMethods.InMemoryPaymentMethodRepository;
import be.cozmix.cashbook.data.utils.RoomCallback;
import be.cozmix.cashbook.model.GadgetOrder;
import be.cozmix.cashbook.model.Transaction;
import be.cozmix.cashbook.model.TransactionProductCrossRef;
import be.cozmix.cashbook.ui.adapters.GadgetOrderAdapter;
import be.cozmix.cashbook.ui.viewmodels.GadgetOrdersViewModel;

public class GadgetsPaymentActivity extends AppCompatActivity {

    private RecyclerView mRecyclerView;
    private GadgetOrderAdapter mGadgetOrderAdapter;
    private GadgetOrdersViewModel mViewModel;
    private Button payconiq;
    private Button addGadget;
    private RadioGroup radioGroup;
    private EditText mComment;
    private TextView mPrice;

    private static final String TAG = "MyActivity";

    private Button cancel;
    private Button confirm;

    private List<GadgetOrder> mGadgetOrders = new ArrayList<>();

    private RoomCashBookRepository cashBookRepository;
    private final be.cozmix.cashbook.utils.UserManager userManager = be.cozmix.cashbook.utils.UserManager.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gadgets_payment);
        cashBookRepository = RoomCashBookRepository.getInstance(getApplicationContext());
        radioGroup = findViewById(R.id.radio_payment);
        mComment  = findViewById(R.id.txt_gadgets_remark);
        mPrice = findViewById(R.id.total_price);
        String priceText = "TOTAAL "+InMemoryGadgetOrderRepository.getInstance().calculateTotalPrice()+" EURO";
        mPrice.setText(priceText);
        setLayout();
        initViewModel();
        initRecyclerView();
    }

    private void initViewModel() {
        Observer<List<GadgetOrder>> gadgetOrdersObserver = new Observer<List<GadgetOrder>>() {

            @Override
            public void onChanged(List<GadgetOrder> gadgetOrders) {
                Log.v(TAG, "index=" + gadgetOrders);
                mGadgetOrders.clear();
                mGadgetOrders.addAll(gadgetOrders);
                notifyAdapter(gadgetOrders);
            }
        };

        mViewModel = ViewModelProviders.of(this).get(GadgetOrdersViewModel.class);
        mViewModel.getGadgetOrders().observe(this, gadgetOrdersObserver);
    }

    private void notifyAdapter(List<GadgetOrder> gadgets) {
        if (mGadgetOrderAdapter == null) {
            mGadgetOrderAdapter = new GadgetOrderAdapter(getApplicationContext(), gadgets);
            mRecyclerView.setAdapter(mGadgetOrderAdapter);
        } else {
            mGadgetOrderAdapter.notifyDataSetChanged();
        }
    }

    private void initRecyclerView() {
        mRecyclerView = findViewById(R.id.recyclerview_gadgetsorders);

        mGadgetOrderAdapter = new GadgetOrderAdapter(this, mGadgetOrders);

        mRecyclerView.setAdapter(mGadgetOrderAdapter);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    private void setLayout() {

        payconiq = findViewById(R.id.btn_payconiq);
        addGadget = findViewById(R.id.btn_add_gadget);

        cancel = findViewById(R.id.btn_cancel_payment);
        confirm = findViewById(R.id.btn_confirm_payment);

        cancel.setOnClickListener(v -> {
            finish();
        });
        payconiq.setOnClickListener(v -> {
            Intent launchIntent = getPackageManager().getLaunchIntentForPackage("mobi.inthepocket.bcmc.bancontact");
            if (launchIntent != null) {
                startActivity(launchIntent);
            } else {
                Toast.makeText(this, "There is no package available in android", Toast.LENGTH_LONG).show();
            }
        });

        addGadget.setOnClickListener(v -> {
            Intent intent = new Intent(getApplication(), GadgetsActivity.class);
            startActivity(intent);
        });

        confirm.setOnClickListener(v -> {
            int selectedId = radioGroup.getCheckedRadioButtonId();

            if (!gadgetsSelected()){
                showToastMessage("Gelieve eerst gadgets te selecteren");
            } else {
                if (checkIfPaymentMethodSelected(selectedId)){
                    RadioButton checked = radioGroup.findViewById(selectedId);
                    double totalPrice = InMemoryGadgetOrderRepository.getInstance().calculateTotalPrice();
                    int payment_id = InMemoryPaymentMethodRepository.getInstance().getPaymentIdByName(checked.getTag().toString());
                    saveGadgets(payment_id);
                }
                else {
                    showToastMessage("Gelieve betaalmethode te selecteren");
                }
            }




        });
    }

    public void saveGadgets(int paymentId){
        LocalDateTime dateTime = LocalDateTime.now();
        String comment = mComment.getText().toString();
        int userId = userManager.getUser().getId();

        List<GadgetOrder> gadgetOrders = InMemoryGadgetOrderRepository.getInstance().getGadgetOrders();
        Transaction transaction = new Transaction(0, dateTime, comment, InMemoryGadgetOrderRepository.getInstance().calculateTotalPrice(), userId, 2, "", paymentId);

        cashBookRepository.getTransactionEntityManager().addAndGetId(transaction, new RoomCallback() {
            @Override
            public void onSuccess(int id) {
                System.out.println("Success");
                List<TransactionProductCrossRef> transactionProductCrossRefs = new LinkedList<>();
                for (GadgetOrder gadgetOrder : gadgetOrders){
                    transactionProductCrossRefs.add(new TransactionProductCrossRef(
                            id,
                            gadgetOrder.getProductId(),
                            gadgetOrder.getQuantity()
                    ));
                }

                transactionProductCrossRefs.forEach(System.out::println);



                cashBookRepository.getTransactionEntityManager().insertAllTransactionProductCrossReferences(transactionProductCrossRefs);
                Intent intent = new Intent(getApplication(), ConfirmationActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }

    public boolean checkIfPaymentMethodSelected(int id){
        return id != -1;
    }

    public boolean gadgetsSelected(){
        return InMemoryGadgetOrderRepository.getInstance().getGadgetOrders().size() > 0;
    }

    public void showToastMessage(String message){
        CharSequence text = message;
        int duration = Toast.LENGTH_LONG;
        Toast toast = Toast.makeText(getApplicationContext(), text, duration);
        toast.show();
    }
}

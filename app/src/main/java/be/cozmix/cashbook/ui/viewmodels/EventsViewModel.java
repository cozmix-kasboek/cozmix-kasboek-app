package be.cozmix.cashbook.ui.viewmodels;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.time.LocalDate;
import java.util.List;

import be.cozmix.cashbook.data.Repositories;
import be.cozmix.cashbook.data.database.CashBookRepository;
import be.cozmix.cashbook.model.Event;

public class EventsViewModel extends AndroidViewModel {

    private CashBookRepository mRepository;

    public EventsViewModel(@NonNull Application application) {
        super(application);
        mRepository = Repositories.getCashBookRepository(application);
    }

    public LiveData<List<Event>> getEvents() {
        return mRepository.getEventEntityManager().getAllEvents();
    }

    public LiveData<List<Event>> getEvents(LocalDate date) {
        return mRepository.getEventEntityManager().getEventsByDate(date);
    }

}

package be.cozmix.cashbook.ui.activities;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.SpannableString;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import be.cozmix.cashbook.R;
import be.cozmix.cashbook.model.Event;
import be.cozmix.cashbook.ui.viewmodels.EventsViewModel;
import be.cozmix.cashbook.utils.EventHolder;
import be.cozmix.cashbook.utils.UserManager;

public class HomeActivity extends AppCompatActivity {

    private EventsViewModel mViewModel;
    private List<Event> mEvents = new ArrayList<>();

    private TextView dateText;

    private LinearLayout eventButtons;
    private Button addEvent;

    private Button gadgets;
    private Button others;
    private Button transactions;

    private TextView user;
    private Button logout;

    private LocalDate date = LocalDate.now();
    private DateTimeFormatter renderFormatter = DateTimeFormatter.ofPattern("d/M/yyyy");

    private final UserManager userManager = UserManager.getInstance();

    private final EventHolder eventHolder = EventHolder.getInstance();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        setLayout();
        initViewModel();
    }

    private void setLayout() {
        dateText = findViewById(R.id.txt_current_date);
        eventButtons = findViewById(R.id.event_buttons);
        addEvent = findViewById(R.id.btn_event_management);
        user = findViewById(R.id.txt_current_user);
        logout = findViewById(R.id.btn_logout);
        gadgets = findViewById(R.id.btn_gadgets);
        others = findViewById(R.id.btn_other_transactions);
        transactions = findViewById(R.id.btn_transactions);
        dateText.setText(renderFormatter.format(date));
        addEvent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplication(), EventManagementActivity.class));
            }
        });
        user.setText(String.format("%s",
                userManager.getUser().getFirstName()));
        logout.setOnClickListener(v -> {
            userManager.logout();
            Intent intent = new Intent(getApplication(), MainActivity.class);
            startActivity(intent);
        });
        gadgets.setOnClickListener(v -> {
            Intent intent = new Intent(getApplication(), GadgetsActivity.class);
            startActivity(intent);
        });
        others.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplication(), OtherTransactionsActivity.class);
                startActivity(intent);
            }
        });
        transactions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplication(), TransactionsActivity.class);
                startActivity(intent);
            }
        });
    }

    private void initViewModel() {
        Observer<List<Event>> eventsObserver = new Observer<List<Event>>() {
            @Override
            public void onChanged(List<Event> events) {
                mEvents.clear();
                eventButtons.removeAllViews();
                eventButtons.invalidate();
                mEvents.addAll(events);
                render(events);
            }
        };

        mViewModel = ViewModelProviders.of(this).get(EventsViewModel.class);
        mViewModel.getEvents(date).observe(this, eventsObserver);
    }

    private void render(List<Event> events) {
        int i = 0;
        if (events != null && events.size() > i) {
            while (i < events.size()) {
                Event event = events.get(i);

                Button button = new Button(this);
                SpannableString spannableString = new SpannableString(event.getTicketsSold()+ " " + getSingularOrPluralString(event.getTicketsSold()));
                button.setText(event.getName() + " - " + event.getTimeString() + System.getProperty("line.separator") +  spannableString);


                button.setTextColor(Color.BLACK);
                button.setTextSize(18);
                button.setPadding(24,24,24,24);
                button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        eventHolder.setEvent(event);
                        Intent intent = new Intent(getApplication(), TicketsActivity.class);
                        startActivity(intent);
                    }
                });
                eventButtons.addView(button, i);
                i++;

            }
        }
    }

    private String getSingularOrPluralString(int amount){
        return amount != 1 ? "verkochte tickets" : "verkocht ticket";
    }

}

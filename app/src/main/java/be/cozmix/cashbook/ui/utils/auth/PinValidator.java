package be.cozmix.cashbook.ui.utils.auth;

import java.util.Locale;

import be.cozmix.cashbook.ui.utils.Validator;

public class PinValidator implements Validator {

    private static final int REQUIRED_PIN_LENGTH = 4;

    private static final String ERROR_PIN_REQUIRED = "PIN code is vereist.";
    private static final String ERROR_PIN_TOO_SHORT = String.format(Locale.US,
            "%d cijfers nodig.", REQUIRED_PIN_LENGTH);
    private static final String ERROR_PIN_TOO_LONG = "PIN code is te lang.";

    private String error;

    @Override
    public boolean validate(final String pin) {
        int length = pin.trim().length();

        if (length == REQUIRED_PIN_LENGTH) {
            error = null;
            return true;
        }

        if (length <= 0) {
            error = ERROR_PIN_REQUIRED;
        } else if (length < REQUIRED_PIN_LENGTH) {
            error = ERROR_PIN_TOO_SHORT;
        } else {
            error = ERROR_PIN_TOO_LONG;
        }

        return false;
    }

    @Override
    public String getErrorMessage() {
        return error;
    }

}

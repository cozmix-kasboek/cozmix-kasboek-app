package be.cozmix.cashbook.ui.viewmodels;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import org.json.JSONException;
import org.json.JSONObject;

import be.cozmix.cashbook.data.Repositories;
import be.cozmix.cashbook.data.auth.Authorization;
import be.cozmix.cashbook.data.database.RoomCashBookRepository;
import be.cozmix.cashbook.model.User;
import be.cozmix.cashbook.ui.utils.Validator;
import be.cozmix.cashbook.ui.utils.auth.LoginFormState;
import be.cozmix.cashbook.ui.utils.auth.LoginResult;
import be.cozmix.cashbook.ui.utils.auth.PinValidator;
import be.cozmix.cashbook.utils.Callback;
import be.cozmix.cashbook.utils.CashBookException;

public class LoginViewModel extends AndroidViewModel {
    private Validator validator = new PinValidator();

    private MutableLiveData<LoginFormState> loginFormState = new MutableLiveData<>();
    private MutableLiveData<LoginResult> loginResult = new MutableLiveData<>();
    private LiveData<User> mUser;

    private Authorization volleyAuthRepository;

    public LoginViewModel(@NonNull Application application, int id) {
        super(application);
        mUser = RoomCashBookRepository.getInstance(application).getUserEntityManager().getUserById(id);
        volleyAuthRepository = Repositories.getAuthorization(application);
    }

    public MutableLiveData<LoginFormState> getLoginFormState() {
        return loginFormState;
    }

    public MutableLiveData<LoginResult> getLoginResult() {
        return loginResult;
    }

    public LiveData<User> getUser() {
        return mUser;
    }

    public void login(String pin) {
        volleyAuthRepository.login(pin, new Callback() {
            @Override
            public void onResponse(JSONObject response) throws JSONException {
                boolean accepted = response.getBoolean("accepted");
                if (accepted) {
                    loginResult.setValue(new LoginResult(true));
                } else {
                    loginResult.setValue(new LoginResult("Pin incorrect."));
                }
            }

            @Override
            public void onFailure(CashBookException exc) {
                loginResult.setValue(new LoginResult("Login mislukt."));
            }
        });
    }

    public boolean isDataValid(final String pin) {
        boolean isDataValid = validator.validate(pin);
        if (isDataValid) {
            loginFormState.setValue( new LoginFormState(true) );
        } else {
            loginFormState.setValue( new LoginFormState(validator.getErrorMessage()) );
        }
        return isDataValid;
    }

}

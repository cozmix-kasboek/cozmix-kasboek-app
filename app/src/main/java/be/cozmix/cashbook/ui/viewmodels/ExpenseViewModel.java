package be.cozmix.cashbook.ui.viewmodels;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

import be.cozmix.cashbook.data.database.CashBookRepository;
import be.cozmix.cashbook.data.database.RoomCashBookRepository;
import be.cozmix.cashbook.data.products.IProductEntityManager;
import be.cozmix.cashbook.model.Product;

public class ExpenseViewModel extends AndroidViewModel {

    private IProductEntityManager productManager;
    private LiveData<List<Product>> mExpenses;

    public ExpenseViewModel(@NonNull Application application) {
        super(application);
        CashBookRepository repository = RoomCashBookRepository.getInstance(application);
        this.productManager= repository.getProductEntityManager();
        mExpenses = productManager.getExpenses();
    }

    public LiveData<List<Product>> getExpenses() { return mExpenses; }
}

package be.cozmix.cashbook.ui.viewmodels;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import be.cozmix.cashbook.data.Repositories;
import be.cozmix.cashbook.data.database.CashBookRepository;
import be.cozmix.cashbook.model.Event;
import be.cozmix.cashbook.ui.utils.Validator;
import be.cozmix.cashbook.ui.utils.events.EventEditFormState;
import be.cozmix.cashbook.ui.utils.events.EventNameValidator;
import be.cozmix.cashbook.ui.utils.events.EventTimeValidator;

public class EventEditViewModel extends AndroidViewModel {

    private Validator nameValidator = new EventNameValidator();
    private Validator timeValidator = new EventTimeValidator();

    private MutableLiveData<EventEditFormState> eventFormState = new MutableLiveData<>();
    private CashBookRepository mRepository;
    private LiveData<Event> mEvent;

    public EventEditViewModel(@NonNull Application application, String id) {
        super(application);
        mRepository = Repositories.getCashBookRepository(application);
        mEvent = mRepository.getEventEntityManager().getEventById(id);
    }

    public MutableLiveData<EventEditFormState> getEventFormState() {
        return eventFormState;
    }

    public LiveData<Event> getEvent() {
        return mEvent;
    }

    public void update(Event event) {
        mRepository.getEventEntityManager().updateEvent(event);
    }

    public void delete(Event event) {
        mRepository.getEventEntityManager().deleteEvent(event);
    }

    public boolean isDataValid(final String name, final String time) {
        boolean isNameValid = nameValidator.validate(name);
        boolean isTimeValid = timeValidator.validate(time);

        if (!isNameValid) {
            eventFormState.setValue(
                    new EventEditFormState(nameValidator.getErrorMessage(), null));
        } else if (!isTimeValid) {
            eventFormState.setValue(
                    new EventEditFormState(null, timeValidator.getErrorMessage()));
        } else {
            eventFormState.setValue(new EventEditFormState(true));
        }

        return isNameValid && isTimeValid;
    }

}

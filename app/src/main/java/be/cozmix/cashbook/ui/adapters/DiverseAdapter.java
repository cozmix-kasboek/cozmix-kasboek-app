package be.cozmix.cashbook.ui.adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import be.cozmix.cashbook.model.Product;

public class DiverseAdapter extends ArrayAdapter<Product> {

    private Context context;
    private List<Product> mDiverseProducts;

    public DiverseAdapter(Context context, int textViewResourceId, List<Product> diverse){
        super(context, textViewResourceId, diverse);
        this.context = context;
        this.mDiverseProducts = diverse;
    }

    @Override
    public int getCount() {
        return mDiverseProducts.size();
    }

    @Override
    public Product getItem(int position) {
        return mDiverseProducts.get(position);
    }

    @Override
    public long getItemId(int position) {
        return mDiverseProducts.get(position).getProductId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        TextView label = (TextView) super.getView(position, convertView, parent);
        label.setTextColor(Color.BLACK);
        label.setText(mDiverseProducts.get(position).getName());
        return label;
    }

    @Override
    public View getDropDownView(int position, View convertView,
                                ViewGroup parent) {
        TextView label = (TextView) super.getDropDownView(position, convertView, parent);
        label.setTextColor(Color.BLACK);
        System.out.println(mDiverseProducts.get(position).getName());
        label.setText(mDiverseProducts.get(position).getName());

        return label;
    }

    public void setExpenses(List<Product> expenses){
        mDiverseProducts = expenses;
    }
}

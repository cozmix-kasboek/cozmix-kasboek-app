package be.cozmix.cashbook.ui.utils.events;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

import be.cozmix.cashbook.ui.utils.Validator;

public class EventTimeValidator implements Validator {

    private final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("H:mm");

    private static final String ERROR_TIME_REQUIRED = "Tijdstip is vereist.";
    private static final String ERROR_TIME_FORMAT = "Het tijdstip is onduidelijk.";

    private String error;

    @Override
    public boolean validate(String input) {
        if (input == null ||input.trim().length() <= 0) {
            error = ERROR_TIME_REQUIRED;
            return false;
        }

        try {
            LocalTime.parse(input, formatter);
        } catch (DateTimeParseException exc) {
            error = ERROR_TIME_FORMAT;
            return false;
        }

        error = null;
        return true;
    }

    @Override
    public String getErrorMessage() {
        return error;
    }

}

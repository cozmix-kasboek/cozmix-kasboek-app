package be.cozmix.cashbook.ui.utils.events;

import androidx.annotation.Nullable;

// Data validation state of the event add form.
public class EventAddFormState {

    @Nullable
    private String nameError;
    @Nullable
    private String dateError;
    @Nullable
    private String timeError;

    private boolean isDataValid;

    public EventAddFormState(@Nullable String nameError,
                             @Nullable String dateError,
                             @Nullable String timeError) {
        this.nameError = nameError;
        this.dateError = dateError;
        this.timeError = timeError;
        isDataValid = false;
    }

    public EventAddFormState(boolean isDataValid) {
        this.nameError = null;
        this.dateError = null;
        this.timeError = null;
        this.isDataValid = isDataValid;
    }

    @Nullable
    public String getNameError() {
        return nameError;
    }

    @Nullable
    public String getDateError() {
        return dateError;
    }

    @Nullable
    public String getTimeError() {
        return timeError;
    }

    public boolean isDataValid() {
        return isDataValid;
    }

    public boolean hasNameError() {
        return nameError != null;
    }

    public boolean hasDateError() {
        return dateError != null;
    }

    public boolean hasTimeError() {
        return timeError != null;
    }

}

package be.cozmix.cashbook.ui.viewmodels;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

import be.cozmix.cashbook.data.database.RoomCashBookRepository;
import be.cozmix.cashbook.model.Product;

public class GadgetsViewModel extends AndroidViewModel {

    private LiveData<List<Product>> mGadgets;

    public GadgetsViewModel(Application application) {
        super(application);
        mGadgets = RoomCashBookRepository.getInstance(application).getProductEntityManager().getGadgets();
    }

    public LiveData<List<Product>> getGadgets() {
        return mGadgets;
    }
}

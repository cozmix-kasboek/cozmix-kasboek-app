package be.cozmix.cashbook.ui.activities;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Locale;
import java.util.UUID;

import be.cozmix.cashbook.R;
import be.cozmix.cashbook.model.Event;
import be.cozmix.cashbook.ui.utils.events.EventAddFormState;
import be.cozmix.cashbook.ui.viewmodels.EventAddViewModel;

public class EventAddActivity extends AppCompatActivity {

    private EventAddViewModel mViewModel;

    private TextView error;

    private EditText name;
    private DatePicker date;
    private EditText time;

    private String nameString;
    private String dateString;
    private String timeString;

    private TextView cancel;
    private TextView confirm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_add);

        setLayout();
        initViewModel();
        initDataWatcher();
    }

    private void setLayout() {
        this.error = findViewById(R.id.txt_event_error);
        this.name = findViewById(R.id.txt_event_description);
        this.date = findViewById(R.id.txt_event_date);
        this.time = findViewById(R.id.txt_event_time);
        this.cancel = findViewById(R.id.cancel);
        this.confirm = findViewById(R.id.confirm);
        date.setFirstDayOfWeek(Calendar.MONDAY);
        dateString = String.format(Locale.US, "%d/%d/%d",
                date.getDayOfMonth(), date.getMonth() + 1, date.getYear());
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void initViewModel() {
        Observer<EventAddFormState> eventAddFormStateObserver = new Observer<EventAddFormState>() {
            @Override
            public void onChanged(EventAddFormState eventAddFormState) {
                if (eventAddFormState == null) {
                    return;
                }
                enableConfirm(eventAddFormState.isDataValid());
                if (!eventAddFormState.isDataValid()) {
                    if (eventAddFormState.hasNameError()) {
                        error.setText(eventAddFormState.getNameError());
                    }
                    if (eventAddFormState.hasDateError()) {
                        error.setText(eventAddFormState.getDateError());
                    }
                    if (eventAddFormState.hasTimeError()) {
                        error.setText(eventAddFormState.getTimeError());
                    }
                    error.setVisibility(View.VISIBLE);
                } else {
                    error.setVisibility(View.INVISIBLE);
                }
            }
        };

        mViewModel = ViewModelProviders.of(this).get(EventAddViewModel.class);
        mViewModel.getEventFormState().observe(this, eventAddFormStateObserver);
    }

    private void initDataWatcher() {
        TextWatcher afterTextChangedListener = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // Android recommends you leave this method empty.
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // Android recommends you leave this method empty.
            }

            @Override
            public void afterTextChanged(Editable s) {
                nameString = name.getText().toString();
                timeString = time.getText().toString();

                mViewModel.isDataValid(nameString, dateString, timeString);
            }
        };

        name.addTextChangedListener(afterTextChangedListener);
        time.addTextChangedListener(afterTextChangedListener);

        date.setOnDateChangedListener(new DatePicker.OnDateChangedListener() {
            @Override
            public void onDateChanged(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                dateString = String.format(Locale.US, "%d/%d/%d",
                        dayOfMonth, monthOfYear + 1, year);
                mViewModel.isDataValid(nameString, dateString, timeString);
            }
        });

        time.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE &&
                        mViewModel.isDataValid(nameString, dateString, timeString)) {
                    addEvent();
                    finish();
                }
                return false;
            }
        });
        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mViewModel.isDataValid(nameString, dateString, timeString)) {
                    addEvent();
                    finish();
                }
            }
        });
    }

    private void addEvent() {
        String uuid = UUID.randomUUID().toString();
        DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("d/M/yyyy");
        DateTimeFormatter timeFormatter = DateTimeFormatter.ofPattern("H:mm");
        LocalDate localDate = LocalDate.parse(dateString, dateFormatter);
        LocalTime localTime = LocalTime.parse(timeString, timeFormatter);

        Event event = new Event(uuid, nameString, localDate, localTime);
        mViewModel.add(event);
    }

    private void enableConfirm(boolean enable) {
        int color = R.color.colorDisabled;
        int drawable = R.drawable.ic_check_gray;
        if (enable) {
            color = R.color.colorBlack;
            drawable = R.drawable.ic_check_black;
        }

        confirm.setEnabled(enable);
        confirm.setTextColor(getResources().getColor(color, getTheme()));
        confirm.setCompoundDrawables(null,
                getResources().getDrawable(drawable, getTheme()), null, null);
    }

}

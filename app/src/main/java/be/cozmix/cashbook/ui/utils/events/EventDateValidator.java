package be.cozmix.cashbook.ui.utils.events;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

import be.cozmix.cashbook.ui.utils.Validator;

public class EventDateValidator implements Validator {

    private final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d/M/yyyy");

    private static final String ERROR_DATE_REQUIRED = "Datum is vereist.";
    private static final String ERROR_DATE_FORMAT = "De datum is onduidelijk.";

    private String error;

    @Override
    public boolean validate(String input) {
        if (input == null ||input.trim().length() <= 0) {
            error = ERROR_DATE_REQUIRED;
            return false;
        }

        try {
            LocalDate.parse(input, formatter);
        } catch (DateTimeParseException exc) {
            error = ERROR_DATE_FORMAT;
            return false;
        }

        error = null;
        return true;
    }

    @Override
    public String getErrorMessage() {
        return error;
    }

}

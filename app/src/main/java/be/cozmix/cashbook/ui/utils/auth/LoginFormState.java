package be.cozmix.cashbook.ui.utils.auth;

import androidx.annotation.Nullable;

// Data validation state of the login form.
public class LoginFormState {

    @Nullable
    private String pinError;
    private boolean isDataValid;

    public LoginFormState(@Nullable String pinError) {
        this.pinError = pinError;
        this.isDataValid = false;
    }

    public LoginFormState(boolean isDataValid) {
        this.pinError = null;
        this.isDataValid = isDataValid;
    }

    @Nullable
    public String getPinError() {
        return pinError;
    }

    public boolean isDataValid() {
        return isDataValid;
    }

    public boolean hasPinError() {
        return pinError != null;
    }

}

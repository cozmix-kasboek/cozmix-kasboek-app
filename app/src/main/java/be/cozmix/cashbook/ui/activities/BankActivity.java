package be.cozmix.cashbook.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.time.LocalDateTime;
import java.util.Locale;

import be.cozmix.cashbook.R;
import be.cozmix.cashbook.data.database.RoomCashBookRepository;
import be.cozmix.cashbook.data.transactions.ITransactionEntityManager;
import be.cozmix.cashbook.model.Transaction;
import be.cozmix.cashbook.utils.UserManager;

public class BankActivity extends AppCompatActivity {

    private static final String TAG = BankActivity.class.getSimpleName();
    private final String to_bank = "€%.2f van kas naar bank.";
    private final String from_bank = "€%.2f van bank naar kas.";

    private EditText amount;
    private EditText remark;

    private RadioButton toBank;
    private RadioButton fromBank;

    private TextView cancel;
    private TextView confirm;

    private ITransactionEntityManager transactionManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bank);
        transactionManager = RoomCashBookRepository.getInstance(this).getTransactionEntityManager();
        setLayout();
    }

    private void setLayout() {
        amount = findViewById(R.id.txt_bank_amount);
        remark = findViewById(R.id.txt_bank_remark);
        toBank = findViewById(R.id.to_bank);
        fromBank = findViewById(R.id.from_bank);
        cancel = findViewById(R.id.cancel);
        confirm = findViewById(R.id.confirm);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (amount.getText().toString().trim().length() > 0) {
                    try {
                        double price = Double.parseDouble(amount.getText().toString());
                        String description = String.format(Locale.US, from_bank, price);
                        if (toBank.isChecked() && !fromBank.isChecked()) {
                            description = String.format(Locale.US, to_bank, price);
                            price = -price;
                        }
                        Transaction transaction = new Transaction(LocalDateTime.now(),
                                description, price,
                                UserManager.getInstance().getUser().getId(),
                                3, "", 1);
                        transactionManager.add(transaction);
                        startActivity(new Intent(getApplication(), ConfirmationActivity.class));
                    } catch (NumberFormatException exc) {
                        Log.e(TAG, "onClick: Error parsing amount.", exc);
                        Toast.makeText(getApplicationContext(), "Bedrag onduidelijk!", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }

}

package be.cozmix.cashbook.ui.utils.events;

import androidx.annotation.Nullable;

public class EventEditFormState {

    @Nullable
    private String nameError;
    @Nullable
    private String timeError;

    private boolean isDataValid;

    public EventEditFormState(@Nullable String nameError, @Nullable String timeError) {
        this.timeError = timeError;
        this.nameError = nameError;
        isDataValid = false;
    }

    public EventEditFormState(boolean isDataValid) {
        this.nameError = null;
        this.timeError = null;
        this.isDataValid = isDataValid;
    }

    @Nullable
    public String getNameError() {
        return nameError;
    }

    @Nullable
    public String getTimeError() {
        return timeError;
    }

    public boolean isDataValid() {
        return isDataValid;
    }

    public boolean hasNameError() {
        return nameError != null;
    }

    public boolean hasTimeError() {
        return timeError != null;
    }


}

package be.cozmix.cashbook.ui.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import be.cozmix.cashbook.R;
import be.cozmix.cashbook.model.User;
import be.cozmix.cashbook.ui.activities.LoginActivity;

public class UserAdapter extends RecyclerView.Adapter<UserAdapter.UserViewHolder> {

    private Context mContext;
    private List<User> mUsers;

    public UserAdapter(Context mContext, List<User> mUsers) {
        this.mContext = mContext;
        this.mUsers = mUsers;
    }

    @NonNull
    @Override
    public UserViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater mInflater = LayoutInflater.from(mContext);
        View mItemView = mInflater.inflate(R.layout.card_user, parent, false);
        return new UserViewHolder(mItemView);
    }

    @Override
    public void onBindViewHolder(@NonNull UserViewHolder holder, int position) {
        final User mCurrent = mUsers.get(position);
        holder.username.setText(mCurrent.getFirstName());
        holder.color.setBackgroundColor(ContextCompat.getColor(mContext, R.color.colorUserCard));
        holder.userCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, LoginActivity.class);
                intent.putExtra("id", mCurrent.getId());
                mContext.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mUsers.size();
    }

    static class UserViewHolder extends RecyclerView.ViewHolder {
        private CardView userCard;

        private TextView username;
        private View color;

        UserViewHolder(@NonNull View itemView) {
            super(itemView);
            userCard = itemView.findViewById(R.id.user_card);

            username = itemView.findViewById(R.id.user_name);
            color = itemView.findViewById(R.id.user_color);
        }

    }

}

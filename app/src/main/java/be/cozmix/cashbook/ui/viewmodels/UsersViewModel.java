package be.cozmix.cashbook.ui.viewmodels;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

import be.cozmix.cashbook.data.Repositories;
import be.cozmix.cashbook.data.database.CashBookRepository;
import be.cozmix.cashbook.model.User;

public class UsersViewModel extends AndroidViewModel {

    private LiveData<List<User>> mUsers;

    public UsersViewModel(@NonNull Application application) {
        super(application);
        CashBookRepository mRepository = Repositories.getCashBookRepository(application);
        mUsers = mRepository.getUserEntityManager().getAllUsers();
    }

    public LiveData<List<User>> getUsers() {
        return mUsers;
    }

}

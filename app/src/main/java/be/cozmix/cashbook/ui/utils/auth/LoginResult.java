package be.cozmix.cashbook.ui.utils.auth;

import androidx.annotation.Nullable;

// Authentication result: success or error message.
public class LoginResult {

    @Nullable
    private String error;
    private boolean success;

    public LoginResult(@Nullable String error) {
        this.error = error;
    }

    public LoginResult(boolean success) {
        this.success = success;
    }

    @Nullable
    public String getError() {
        return error;
    }

    public boolean hasError() {
        return error != null;
    }

    public boolean hasSuccess() {
        return success;
    }

}

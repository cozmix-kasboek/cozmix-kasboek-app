package be.cozmix.cashbook.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

import be.cozmix.cashbook.R;
import be.cozmix.cashbook.data.gadgetOrders.InMemoryGadgetOrderRepository;
import be.cozmix.cashbook.data.ticketOrders.InMemoryTicketOrderRepository;

public class ConfirmationActivity extends AppCompatActivity {

    private Button home;
    private Button transactions;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirmation);
        setLayout();
        InMemoryGadgetOrderRepository.getInstance().clearAll();
        InMemoryTicketOrderRepository.getInstance().clearAll();
    }

    private void setLayout() {
        home = findViewById(R.id.btn_back_to_homepage);
        transactions = findViewById(R.id.btn_to_transactions);
        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplication(), HomeActivity.class));
                finish();
            }
        });
        transactions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplication(), TransactionsActivity.class));
                finish();
            }
        });
    }

}

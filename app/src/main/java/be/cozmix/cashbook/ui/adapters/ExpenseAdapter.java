package be.cozmix.cashbook.ui.adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import be.cozmix.cashbook.model.Product;

public class ExpenseAdapter extends ArrayAdapter<Product>{

    private Context context;
    private List<Product> mExpenseProducts;

    public ExpenseAdapter(Context context, int textViewResourceId, List<Product> expenses){
        super(context, textViewResourceId, expenses);
        this.context = context;
        this.mExpenseProducts = expenses;
    }

    @Override
    public int getCount() {
        return mExpenseProducts.size();
    }

    @Override
    public Product getItem(int position) {
        return mExpenseProducts.get(position);
    }

    @Override
    public long getItemId(int position) {
        return mExpenseProducts.get(position).getProductId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        TextView label = (TextView) super.getView(position, convertView, parent);
        label.setTextColor(Color.BLACK);
        label.setText(mExpenseProducts.get(position).getName());
        return label;
    }

    @Override
    public View getDropDownView(int position, View convertView,
                                ViewGroup parent) {
        TextView label = (TextView) super.getDropDownView(position, convertView, parent);
        label.setTextColor(Color.BLACK);
        System.out.println(mExpenseProducts.get(position).getName());
        label.setText(mExpenseProducts.get(position).getName());

        return label;
    }

    public void setExpenses(List<Product> expenses){
        mExpenseProducts = expenses;
    }
}

package be.cozmix.cashbook.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import be.cozmix.cashbook.R;
import be.cozmix.cashbook.model.User;
import be.cozmix.cashbook.ui.utils.auth.LoginFormState;
import be.cozmix.cashbook.ui.utils.auth.LoginResult;
import be.cozmix.cashbook.ui.viewmodels.LoginViewModel;
import be.cozmix.cashbook.utils.UserManager;
import be.cozmix.cashbook.utils.ViewModelFactory;

public class LoginActivity extends AppCompatActivity {

    private ImageView image;
    private TextView username;
    private EditText pin;
    private TextView error;
    private ProgressBar loadingProgressBar;

    private TextView cancel;
    private TextView confirm;

    private LoginViewModel mViewModel;
    private User mUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        Bundle extras = getIntent().getExtras();
        if (extras == null) {
            Toast.makeText(getApplicationContext(), "Failed.", Toast.LENGTH_SHORT).show();
            finish();
        } else {
            int id = extras.getInt("id");
            setLayout();
            initViewModel(id);
        }
    }

    private void setLayout() {
        image = findViewById(R.id.img_user);
        username = findViewById(R.id.txt_user_name);
        pin = findViewById(R.id.pin);
        error = findViewById(R.id.txt_pin_error);
        loadingProgressBar = findViewById(R.id.loading);
        cancel = findViewById(R.id.cancel);
        confirm = findViewById(R.id.confirm);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void initViewModel(int id) {
        Observer<LoginFormState> loginFormStateObserver = new Observer<LoginFormState>() {
            @Override
            public void onChanged(LoginFormState loginFormState) {
                if (loginFormState == null) {
                    return;
                }
                if (loginFormState.hasPinError()) {
                    error.setText(loginFormState.getPinError());
                    error.setVisibility(View.VISIBLE);
                } else {
                    error.setVisibility(View.INVISIBLE);
                }
            }
        };

        Observer<LoginResult> loginResultObserver = new Observer<LoginResult>() {
            @Override
            public void onChanged(LoginResult loginResult) {
                if (loginResult == null) {
                    return;
                }
                loadingProgressBar.setVisibility(View.GONE);
                if (loginResult.hasError()) {
                    showLoginFailed(loginResult.getError());
                }
                if (loginResult.hasSuccess()) {
                    // The admin login was a success!
                    UserManager.getInstance().setUser(mUser);
                    startActivity(new Intent(getApplication(), HomeActivity.class));
                }
            }
        };

        Observer<User> userObserver = new Observer<User>() {
            @Override
            public void onChanged(User user) {
                mUser = user;
                render(user);
            }
        };

        mViewModel = ViewModelProviders.of(this,
                new ViewModelFactory(getApplication(), id)).get(LoginViewModel.class);
        mViewModel.getUser().observe(this, userObserver);
        mViewModel.getLoginFormState().observe(this, loginFormStateObserver);
        mViewModel.getLoginResult().observe(this, loginResultObserver);
    }

    private void initTextWatcher() {
        TextWatcher afterTextChangedListener = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // Android recommends you leave this method empty.
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // Android recommends you leave this method empty.
            }

            @Override
            public void afterTextChanged(Editable s) {
                mViewModel.isDataValid(pin.getText().toString());
            }
        };
        pin.addTextChangedListener(afterTextChangedListener);
        pin.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE &&
                        mViewModel.isDataValid(pin.getText().toString())) {
                    loadingProgressBar.setVisibility(View.VISIBLE);
                    mViewModel.login(pin.getText().toString());
                }
                return false;
            }
        });
        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mViewModel.isDataValid(pin.getText().toString())) {
                    loadingProgressBar.setVisibility(View.VISIBLE);
                    mViewModel.login(pin.getText().toString());
                }
            }
        });
    }

    private void render(User user) {
        image.setImageResource(R.drawable.cozmix_logo);
        username.setText(user.getName());
        if (user.isAdmin()) {
            pin.setVisibility(View.VISIBLE);
            initTextWatcher();
        } else {
            confirm.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    UserManager.getInstance().setUser(mUser);
                    startActivity(new Intent(getApplication(), HomeActivity.class));
                }
            });
        }
    }

    private void showLoginFailed(String message) {
        error.setText(message);
        error.setVisibility(View.VISIBLE);
    }

}

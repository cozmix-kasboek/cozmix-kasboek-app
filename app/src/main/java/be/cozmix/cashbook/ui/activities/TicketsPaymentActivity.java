package be.cozmix.cashbook.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import be.cozmix.cashbook.R;
import be.cozmix.cashbook.data.database.RoomCashBookRepository;
import be.cozmix.cashbook.data.paymentMethods.InMemoryPaymentMethodRepository;
import be.cozmix.cashbook.data.ticketOrders.InMemoryTicketOrderRepository;
import be.cozmix.cashbook.data.utils.RoomCallback;
import be.cozmix.cashbook.model.TicketOrder;
import be.cozmix.cashbook.model.Transaction;
import be.cozmix.cashbook.model.TransactionProductCrossRef;
import be.cozmix.cashbook.ui.adapters.TicketOrderAdapter;
import be.cozmix.cashbook.ui.viewmodels.TicketsOrderViewModel;
import be.cozmix.cashbook.utils.EventHolder;

public class TicketsPaymentActivity extends AppCompatActivity {
    private RecyclerView mRecyclerView;
    private TicketOrderAdapter mTicketOrderAdapter;
    private TicketsOrderViewModel mViewModel;
    private Button payconiq;
    private Button addTicket;
    private RadioGroup radioGroup;
    private EditText mComment;
    private TextView mPrice;
    private TextView mEventTitle;

    private static final String TAG = "MyActivity";

    private Button cancel;
    private Button confirm;

    private List<TicketOrder> mTicketOrders = new ArrayList<>();

    private RoomCashBookRepository cashBookRepository;
    private final be.cozmix.cashbook.utils.UserManager userManager = be.cozmix.cashbook.utils.UserManager.getInstance();

    private final EventHolder eventHolder = EventHolder.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tickets_payment);
        cashBookRepository = RoomCashBookRepository.getInstance(getApplicationContext());
        radioGroup = findViewById(R.id.radio_payment);
        mComment  = findViewById(R.id.txt_tickets_remark);
        mEventTitle = findViewById(R.id.event_title);
        mPrice = findViewById(R.id.total_price);
        String priceText = "TOTAAL "+ InMemoryTicketOrderRepository.getInstance().calculateTotalPrice()+" EURO";
        mEventTitle.setText(eventHolder.getEvent().getName()+ " | "+ eventHolder.getEvent().getDatetimeString());
        mPrice.setText(priceText);
        setLayout();
        initViewModel();
        initRecyclerView();
    }

    private void initViewModel() {
        Observer<List<TicketOrder>> ticketOrderObserver = new Observer<List<TicketOrder>>() {

            @Override
            public void onChanged(List<TicketOrder> ticketOrders) {
                Log.v(TAG, "index=" + ticketOrders);
                mTicketOrders.clear();
                mTicketOrders.addAll(ticketOrders);
                notifyAdapter(ticketOrders);
            }
        };

        mViewModel = ViewModelProviders.of(this).get(TicketsOrderViewModel.class);
        mViewModel.getTicketOrders().observe(this, ticketOrderObserver);
    }

    private void notifyAdapter(List<TicketOrder> tickets) {
        if (mTicketOrderAdapter == null) {
            mTicketOrderAdapter = new TicketOrderAdapter(getApplicationContext(), tickets);
            mRecyclerView.setAdapter(mTicketOrderAdapter);
        } else {
            mTicketOrderAdapter.notifyDataSetChanged();
        }
    }

    private void initRecyclerView() {
        mRecyclerView = findViewById(R.id.recyclerview_ticketsorders);

        mTicketOrderAdapter = new TicketOrderAdapter(this, mTicketOrders);

        mRecyclerView.setAdapter(mTicketOrderAdapter);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    private void setLayout() {

        payconiq = findViewById(R.id.btn_payconiq);
        addTicket = findViewById(R.id.btn_add_ticket);

        cancel = findViewById(R.id.btn_cancel_payment);
        confirm = findViewById(R.id.btn_confirm_payment);

        cancel.setOnClickListener(v -> {
            finish();
        });
        payconiq.setOnClickListener(v -> {
            Intent launchIntent = getPackageManager().getLaunchIntentForPackage("mobi.inthepocket.bcmc.bancontact");
            if (launchIntent != null) {
                startActivity(launchIntent);
            } else {
                Toast.makeText(this, "There is no package available in android", Toast.LENGTH_LONG).show();
            }
        });

        addTicket.setOnClickListener(v -> {
            Intent intent = new Intent(getApplication(), TicketsActivity.class);
            startActivity(intent);
        });

        confirm.setOnClickListener(v -> {
            int selectedId = radioGroup.getCheckedRadioButtonId();

            if (!ticketsSelected()){
                showToastMessage("Gelieve eerst tickets te selecteren");
            } else {
                if (checkIfPaymentMethodSelected(selectedId)){
                    RadioButton checked = radioGroup.findViewById(selectedId);
                    double totalPrice = InMemoryTicketOrderRepository.getInstance().calculateTotalPrice();
                    int payment_id = InMemoryPaymentMethodRepository.getInstance().getPaymentIdByName(checked.getTag().toString());
                    saveTickets(payment_id);
                }
                else {
                    showToastMessage("Gelieve betaalmethode te selecteren");
                }
            }




        });
    }

    public void saveTickets(int paymentId){
        LocalDateTime dateTime = LocalDateTime.now();
        String comment = mComment.getText().toString();
        int userId = userManager.getUser().getId();
        String eventId = eventHolder.getEvent().getId();
        int amountOfTickets = InMemoryTicketOrderRepository.getInstance().getTicketAmount();
        List<TicketOrder> ticketOrders = InMemoryTicketOrderRepository.getInstance().getTicketOrders();
        Transaction transaction = new Transaction(0, dateTime, comment, InMemoryTicketOrderRepository.getInstance().calculateTotalPrice(), userId, 1, eventId, paymentId);

        cashBookRepository.getTransactionEntityManager().addAndGetId(transaction, new RoomCallback() {
            @Override
            public void onSuccess(int id) {
                System.out.println("Success");
                eventHolder.getEvent().addTicketAmountOfOrderToTicketsSold(amountOfTickets);
                cashBookRepository.getEventEntityManager().updateEvent(eventHolder.getEvent());
                List<TransactionProductCrossRef> transactionProductCrossRefs = new LinkedList<>();
                for (TicketOrder ticketOrder : ticketOrders){
                    transactionProductCrossRefs.add(new TransactionProductCrossRef(
                            id,
                            ticketOrder.getProductId(),
                            ticketOrder.getQuantity()
                    ));
                }

                transactionProductCrossRefs.forEach(System.out::println);



                cashBookRepository.getTransactionEntityManager().insertAllTransactionProductCrossReferences(transactionProductCrossRefs);
                Intent intent = new Intent(getApplication(), ConfirmationActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }

    public boolean checkIfPaymentMethodSelected(int id){
        return id != -1;
    }

    public boolean ticketsSelected(){
        return InMemoryTicketOrderRepository.getInstance().getTicketOrders().size() > 0;  }

    public void showToastMessage(String message){
        CharSequence text = message;
        int duration = Toast.LENGTH_LONG;
        Toast toast = Toast.makeText(getApplicationContext(), text, duration);
        toast.show();
    }

}

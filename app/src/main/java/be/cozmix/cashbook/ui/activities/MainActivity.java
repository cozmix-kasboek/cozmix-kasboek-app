package be.cozmix.cashbook.ui.activities;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.work.BackoffPolicy;
import androidx.work.Constraints;
import androidx.work.ExistingPeriodicWorkPolicy;
import androidx.work.NetworkType;
import androidx.work.PeriodicWorkRequest;
import androidx.work.WorkManager;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import be.cozmix.cashbook.R;
import be.cozmix.cashbook.data.synchronization.workers.ChainInitiationWorker;
import be.cozmix.cashbook.model.User;
import be.cozmix.cashbook.ui.adapters.UserAdapter;
import be.cozmix.cashbook.ui.viewmodels.UsersViewModel;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getSimpleName();

    private RecyclerView mRecyclerView;
    private UserAdapter mAdapter;
    private UsersViewModel mViewModel;

    private List<User> mUsers = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initViewModel();
        initRecyclerView();
        initSynchronization();
    }

    private void initViewModel() {
        Observer<List<User>> usersObserver = new Observer<List<User>>() {
            @Override
            public void onChanged(final List<User> users) {
                mUsers.clear();
                mUsers.addAll(users);
                notifyAdapter(users);
            }
        };

        mViewModel = ViewModelProviders.of(this).get(UsersViewModel.class);
        mViewModel.getUsers().observe(this, usersObserver);
    }

    private void notifyAdapter(List<User> users) {
        if (mAdapter == null) {
            mAdapter = new UserAdapter(getApplicationContext(), users);
            mRecyclerView.setAdapter(mAdapter);
        } else {
            mAdapter.notifyDataSetChanged();
        }
    }

    private void initRecyclerView() {
        mRecyclerView = findViewById(R.id.recyclerview_users);
        mRecyclerView.setLayoutManager(new GridLayoutManager(this, 4));
        mAdapter = new UserAdapter(this, mUsers);
        mRecyclerView.setAdapter(mAdapter);
    }

    private void initSynchronization() {
        Constraints constraints = new Constraints.Builder()
                .setRequiredNetworkType(NetworkType.CONNECTED)
                .setRequiresBatteryNotLow(true)
                .build();

        PeriodicWorkRequest initiateWorkerChainWorkRequest = new PeriodicWorkRequest
                .Builder(ChainInitiationWorker.class, 1, TimeUnit.DAYS)
                .setConstraints(constraints)
                .setBackoffCriteria(
                        BackoffPolicy.LINEAR,
                        PeriodicWorkRequest.MIN_BACKOFF_MILLIS,
                        TimeUnit.MILLISECONDS
                )
                .build();

        WorkManager.getInstance(this).enqueueUniquePeriodicWork(
                ChainInitiationWorker.NAME,
                ExistingPeriodicWorkPolicy.KEEP,
                initiateWorkerChainWorkRequest
        );
    }

}

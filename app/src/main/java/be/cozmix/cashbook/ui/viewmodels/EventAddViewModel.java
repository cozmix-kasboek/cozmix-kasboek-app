package be.cozmix.cashbook.ui.viewmodels;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import be.cozmix.cashbook.data.Repositories;
import be.cozmix.cashbook.data.database.CashBookRepository;
import be.cozmix.cashbook.model.Event;
import be.cozmix.cashbook.ui.utils.Validator;
import be.cozmix.cashbook.ui.utils.events.EventAddFormState;
import be.cozmix.cashbook.ui.utils.events.EventDateValidator;
import be.cozmix.cashbook.ui.utils.events.EventNameValidator;
import be.cozmix.cashbook.ui.utils.events.EventTimeValidator;

public class EventAddViewModel extends AndroidViewModel {

    private Validator nameValidator = new EventNameValidator();
    private Validator dateValidator = new EventDateValidator();
    private Validator timeValidator = new EventTimeValidator();

    private MutableLiveData<EventAddFormState> eventFormState = new MutableLiveData<>();
    private CashBookRepository mRepository;

    public EventAddViewModel(@NonNull Application application) {
        super(application);
        mRepository = Repositories.getCashBookRepository(application);
    }

    public MutableLiveData<EventAddFormState> getEventFormState() {
        return eventFormState;
    }

    public void add(Event event) {
        mRepository.getEventEntityManager().insertEvent(event);
    }

    public boolean isDataValid(final String name, final String date, final String time) {
        boolean isNameValid = nameValidator.validate(name);
        boolean isTimeValid = timeValidator.validate(time);
        boolean isDateValid = dateValidator.validate(date);

        if (!isNameValid) {
            eventFormState.setValue(
                    new EventAddFormState(nameValidator.getErrorMessage(), null, null));
        } else if (!isTimeValid) {
            eventFormState.setValue(
                    new EventAddFormState(null, null, timeValidator.getErrorMessage()));
        } else if (!isDateValid) {
            eventFormState.setValue(
                    new EventAddFormState(null, dateValidator.getErrorMessage(), null));
        } else {
            eventFormState.setValue( new EventAddFormState(true) );
        }

        return isNameValid && isDateValid && isTimeValid;
    }

}

package be.cozmix.cashbook.ui.viewmodels;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.time.LocalDate;
import java.util.List;

import be.cozmix.cashbook.data.database.CashBookRepository;
import be.cozmix.cashbook.data.database.RoomCashBookRepository;
import be.cozmix.cashbook.model.Transaction;

public class TransactionsViewModel extends AndroidViewModel {
    private CashBookRepository mRepository;
    private LiveData<List<Transaction>> mTransactions;


    public TransactionsViewModel(@NonNull Application application) {
        super(application);

        mRepository = RoomCashBookRepository.getInstance(application);
        this.mTransactions = mRepository.getTransactionEntityManager().getAll();
    }

    public LiveData<List<Transaction>> getTransactions() {
        return mTransactions;
    }

    public LiveData<List<Transaction>> getTransactions(LocalDate date) {
        return mRepository.getTransactionEntityManager().getTransactionsByDate(date);
    }

    public void delete(Transaction transaction) {
        mRepository.getTransactionEntityManager().delete(transaction);
    }

}

package be.cozmix.cashbook.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.time.LocalDateTime;

import be.cozmix.cashbook.R;
import be.cozmix.cashbook.data.database.RoomCashBookRepository;
import be.cozmix.cashbook.data.paymentMethods.InMemoryPaymentMethodRepository;
import be.cozmix.cashbook.data.utils.RoomCallback;
import be.cozmix.cashbook.model.Transaction;
import be.cozmix.cashbook.model.TransactionProductCrossRef;

public class MembershipActivity extends AppCompatActivity {

    private LinearLayout form;

    private EditText name;
    private EditText amount;
    private EditText remark;

    private Button payconiq;

    private TextView cancel;
    private TextView confirm;
    private RadioGroup radioGroup;

    private RoomCashBookRepository cashBookRepository;

    private final be.cozmix.cashbook.utils.UserManager userManager = be.cozmix.cashbook.utils.UserManager.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme);
        super.onCreate(savedInstanceState);
        cashBookRepository = RoomCashBookRepository.getInstance(getApplicationContext());
        setContentView(R.layout.activity_membership);
        setLayout();
    }

    public void setLayout() {
        form = findViewById(R.id.form);
        name = findViewById(R.id.txt_membership_name);
        amount = findViewById(R.id.txt_membership_amount);
        remark = findViewById(R.id.txt_membership_remark);
        radioGroup = findViewById(R.id.radio_payment);
        cancel = findViewById(R.id.cancel);
        confirm = findViewById(R.id.confirm);
        payconiq = findViewById(R.id.btn_payconiq);
        payconiq.setOnClickListener(v -> {
            Intent launchIntent = getPackageManager().getLaunchIntentForPackage("mobi.inthepocket.bcmc.bancontact");
            if (launchIntent != null) {
                startActivity(launchIntent);
            } else {
                Toast.makeText(this, "There is no package available in android", Toast.LENGTH_LONG).show();
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int selectedId = radioGroup.getCheckedRadioButtonId();
                if (isPaymentMethodSelected(selectedId) && isNameFilledOut() && isFeeFilledOut()){
                    RadioButton checked = radioGroup.findViewById(selectedId);
                    int paymentId = InMemoryPaymentMethodRepository.getInstance().getPaymentIdByName(checked.getTag().toString());
                    saveMembership(paymentId);
                }
                else {
                    showToastMessage("Gelieve lidnaam en bedrag in te vullen en betaalmethode te selecteren");
                }


            }
        });
    }

    public void saveMembership(int paymentId){

        LocalDateTime dateTime = LocalDateTime.now();
        String membershipMessage = formatMembershipMessage();
        int userId = userManager.getUser().getId();
        int typeId = 5;
        double membershipFee = Double.parseDouble(amount.getText().toString());
        int productId = 45;

        Transaction transaction = new Transaction(0, dateTime, membershipMessage, membershipFee, userId, typeId, "", paymentId);
        System.out.println(transaction.getDate());
        cashBookRepository.getTransactionEntityManager().addAndGetId(transaction, new RoomCallback() {
            @Override
            public void onSuccess(int id) {
                System.out.println("Success");
                TransactionProductCrossRef transactionProductCrossRef = new TransactionProductCrossRef(id, productId, 1);

                cashBookRepository.getTransactionEntityManager().insertTransactionProductCrossReference(transactionProductCrossRef);
                Intent intent = new Intent(getApplication(), ConfirmationActivity.class);
                startActivity(intent);
                finish();
            }
        });

    }
    public boolean isPaymentMethodSelected(int id){
        return id != -1;
    }

    public boolean isNameFilledOut(){
        return name.getText().toString().length() > 0;
    }

    public boolean isFeeFilledOut() {
        return amount.getText().toString().length() > 0;
    }

    public void showToastMessage(String message){
        CharSequence text = message;
        int duration = Toast.LENGTH_LONG;
        Toast toast = Toast.makeText(getApplicationContext(), text, duration);
        toast.show();
    }

    public String formatMembershipMessage(){
        String memberName = name.getText().toString();
        String comment = remark.getText().toString();

        return "Betaling lidgeld door " + memberName + ". Opmerkingen: " + comment;
    }

}

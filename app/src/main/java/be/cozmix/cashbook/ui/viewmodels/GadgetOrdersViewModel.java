package be.cozmix.cashbook.ui.viewmodels;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

import be.cozmix.cashbook.data.gadgetOrders.GadgetOrderRepository;
import be.cozmix.cashbook.data.gadgetOrders.InMemoryGadgetOrderRepository;
import be.cozmix.cashbook.model.GadgetOrder;

public class GadgetOrdersViewModel extends AndroidViewModel {

    private GadgetOrderRepository mRepository;
    private LiveData<List<GadgetOrder>> mGadgetOrders;

    public GadgetOrdersViewModel(@NonNull Application application) {
        super(application);
        this.mRepository = InMemoryGadgetOrderRepository.getInstance();
        this.mGadgetOrders = mRepository.getAll();
    }

    public LiveData<List<GadgetOrder>> getGadgetOrders() { return mGadgetOrders; }
}

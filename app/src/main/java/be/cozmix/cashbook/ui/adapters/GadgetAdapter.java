package be.cozmix.cashbook.ui.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import be.cozmix.cashbook.R;
import be.cozmix.cashbook.data.gadgetOrders.GadgetOrderRepository;
import be.cozmix.cashbook.data.gadgetOrders.InMemoryGadgetOrderRepository;
import be.cozmix.cashbook.model.Product;
import be.cozmix.cashbook.ui.activities.GadgetsPaymentActivity;

public class GadgetAdapter extends RecyclerView.Adapter<GadgetAdapter.GadgetViewHolder> {

    private Context mContext;
    private List<Product> mGadgets;
    private GadgetOrderRepository mRepository;

    public GadgetAdapter(Context context, List<Product> mGadgets) {
        this.mGadgets = mGadgets;
        this.mContext = context;
        mRepository = InMemoryGadgetOrderRepository.getInstance();
    }
    @NonNull
    @Override
    public GadgetViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater mInflater = LayoutInflater.from(mContext);
        View mGadgetView =
                mInflater.inflate(R.layout.card_gadget, parent, false);
        return new GadgetViewHolder(mGadgetView);
    }

    @Override
    public void onBindViewHolder(@NonNull GadgetViewHolder holder, int position) {
        final Product mCurrent = mGadgets.get(position);
        holder.gadgetName.setText(mCurrent.getName());
        String result = "€ "+String.format("%.2f", mCurrent.getPrice());
        holder.gadgetPrice.setText(result);
        holder.gadgetCard.setOnClickListener(v -> {
//                Intent intent = new Intent(mContext, HomeActivity.class);
//                intent.putExtra("id", mCurrent.getProductId());
            Intent intent = new Intent(mContext, GadgetsPaymentActivity.class);
            intent.putExtra("id", mCurrent.getProductId());
            mRepository.addItem(mCurrent.getProductId(), mCurrent.getName(), mCurrent.getPrice());

            mContext.startActivity(intent);
        });
    }

    @Override
    public int getItemCount() {
        return mGadgets.size();
    }


    class GadgetViewHolder extends RecyclerView.ViewHolder {

        private CardView gadgetCard;
        private TextView gadgetName;
        private TextView gadgetPrice;

        GadgetViewHolder(@NonNull View itemView) {
            super(itemView);
            gadgetCard = itemView.findViewById(R.id.gadget_card);
            gadgetName = itemView.findViewById(R.id.gadget_name);
            gadgetPrice = itemView.findViewById(R.id.gadget_price);

        }
    }

}

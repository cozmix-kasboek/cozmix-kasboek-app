package be.cozmix.cashbook.ui.activities;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import be.cozmix.cashbook.R;
import be.cozmix.cashbook.model.Product;
import be.cozmix.cashbook.ui.adapters.GadgetAdapter;
import be.cozmix.cashbook.ui.viewmodels.GadgetsViewModel;

public class GadgetsActivity extends AppCompatActivity {

    private RecyclerView mRecyclerView;
    private GadgetAdapter mAdapter;
    private GadgetsViewModel mViewModel;

    private List<Product> mGadgets = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gadgets);
        initViewModel();
        initRecyclerView();
    }

    private void initViewModel() {
        Observer<List<Product>> gadgetsObserver = new Observer<List<Product>>() {
            @Override
            public void onChanged(final List<Product> gadgets) {
                mGadgets.clear();
                mGadgets.addAll(gadgets);
                notifyAdapter(gadgets);
            }
        };

        mViewModel = ViewModelProviders.of(this).get(GadgetsViewModel.class);
        mViewModel.getGadgets().observe(this, gadgetsObserver);
    }

    private void notifyAdapter(List<Product> gadgets) {
        if (mAdapter == null) {
            mAdapter = new GadgetAdapter(getApplicationContext(), gadgets);
            mRecyclerView.setAdapter(mAdapter);
        } else {
            mAdapter.notifyDataSetChanged();
        }
    }

    private void initRecyclerView() {
        mRecyclerView = findViewById(R.id.recyclerview_gadgets);

        mAdapter = new GadgetAdapter(this, mGadgets);
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setLayoutManager(new GridLayoutManager(this, 2));
    }
}

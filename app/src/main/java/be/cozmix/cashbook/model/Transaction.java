package be.cozmix.cashbook.model;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

import be.cozmix.cashbook.data.utils.LocalDateTimeConverter;


@Entity(tableName = "transactions")
public class Transaction {

    @PrimaryKey(autoGenerate = true)
    @JsonProperty("transaction_id")
    private int transactionId;
    @TypeConverters({LocalDateTimeConverter.class})
    @JsonIgnore
    private LocalDateTime date;

    @Ignore
    @JsonProperty("date")
    private String dateString;

    @JsonProperty("description")
    private String description;

    @ColumnInfo(name = "transaction_amount")
    @JsonProperty("transaction_amount")
    private double transactionAmount;

    @ColumnInfo(name = "user_id")
    @JsonProperty("user_id")
    private int user;
    @ColumnInfo(name = "type_id")
    @JsonProperty("type_id")
    private int type;
    @ColumnInfo(name = "event_id")
    @JsonProperty("event_id")
    private String event;
    @ColumnInfo(name = "payment_method_id")
    @JsonProperty("payment_method_id")
    private int paymentMethod;

    @Ignore
    @JsonIgnore
    public Transaction(LocalDateTime date, String description, double transactionAmount, int user, int type, String event, int paymentMethod) {
        this(0, date, description, transactionAmount, user, type, event, paymentMethod);
    }

    @JsonCreator
    public Transaction(@JsonProperty("transaction_id") int transactionId,
                       LocalDateTime date,
                       @JsonProperty("description") String description,
                       @JsonProperty("payconiq_amount") double transactionAmount,
                       @JsonProperty("user_id") int user,
                       @JsonProperty("type_id") int type,
                       @JsonProperty("event_id") String event,
                       @JsonProperty("payment_method_id") int paymentMethod) {
        this.transactionId = transactionId;
        this.date = date;
        this.dateString = date.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        this.description = description;
        this.transactionAmount = transactionAmount;
        this.user = user;
        this.type = type;
        this.event = event;
        this.paymentMethod = paymentMethod;
    }

    @JsonProperty("transaction_id")
    public int getTransactionId() {
        return transactionId;
    }

    @JsonProperty("date")
    public String getDateString() {
        return dateString;
    }

    public LocalDateTime getDate() {
        return date;
    }

    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    @JsonProperty("payconiq_amount")
    public double getTransactionAmount() {
        return transactionAmount;
    }

    @JsonProperty("user_id")
    public int getUser(){
        return this.user;
    }

    @JsonProperty("type_id")
    public int getType() {
        return type;
    }

    @JsonProperty("event_id")
    public String getEvent() {
        return event;
    }

    @JsonProperty("payment_method")
    public int getPaymentMethod() {
        return paymentMethod;
    }

    @Override
    @NonNull
    public String toString() {
        return String.format(Locale.US, "%d", transactionId);
    }
}

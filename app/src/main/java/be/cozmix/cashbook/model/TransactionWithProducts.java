package be.cozmix.cashbook.model;

import androidx.room.Embedded;
import androidx.room.Junction;
import androidx.room.Relation;

import java.util.List;

public class TransactionWithProducts {
    @Embedded
    public Transaction transaction;

    @Relation(
            parentColumn = "transactionId",
            entity = TransactionProductCrossRef.class,
            entityColumn = "productId",
            associateBy = @Junction(
                    value = TransactionProductCrossRef.class,
                    parentColumn = "transactionId",
                    entityColumn = "productId"
            )
    )
    public List<TransactionProductCrossRef> products;
}

package be.cozmix.cashbook.model;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

import be.cozmix.cashbook.data.utils.LocalDateTimeConverter;

@Entity(tableName = "events")
public class Event {

    @NonNull
    @PrimaryKey
    @JsonProperty("id")
    private String id;
    @JsonProperty("name")
    private String name;
    @Ignore
    @JsonProperty("datetime")
    private String datetimeString;

    @TypeConverters({LocalDateTimeConverter.class})
    private LocalDateTime datetime;

    @Ignore
    @JsonIgnore
    private LocalDate date;
    @Ignore
    @JsonIgnore
    private LocalTime time;

    @JsonIgnore
    private int ticketsSold;

    @JsonCreator
    public Event(String id, String name, LocalDateTime datetime) {
        this.id = id;
        this.name = name;
        this.datetime = datetime;
        this.datetimeString = datetime.format(DateTimeFormatter.ofPattern("yyyy-MM-dd H:mm:ss"));
        this.date = datetime.toLocalDate();
        this.time = datetime.toLocalTime();
    }

    @Ignore
    public Event(String name, LocalDate date, LocalTime time) {
        this("2bf3ffcc-0f50-48a9-a3bb-a19234be29a2", name, date, time);
    }

    @Ignore
    public Event(String id, String name, LocalDate date, LocalTime time) {
        this.id = id;
        this.name = name;
        this.date = date;
        this.time = time;
        this.datetime = LocalDateTime.of(date, time);
        this.datetimeString = datetime.format(DateTimeFormatter.ofPattern("yyyy-M-d H:mm:ss"));
        this.ticketsSold = 0;
    }

    @JsonProperty("id")
    public String getId() {
        return id;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("datetime")
    public String getDatetimeString() {
        return datetimeString;
    }

    public LocalDateTime getDatetime() {
        return datetime;
    }

    public LocalDate getDate() {
        return date;
    }

    public LocalTime getTime() {
        return time;
    }

    public String getTimeString() {
        return datetime.format(DateTimeFormatter.ofPattern("H:mm"));
    }

    public void setDate(LocalDate date) {
        this.date = date;
        refreshDateTime();
    }

    public void setTime(LocalTime time) {
        this.time = time;
        refreshDateTime();
    }

    public void setTicketsSold(int ticketsSold){
        this.ticketsSold = ticketsSold;
    }

    private void refreshDateTime() {
        this.datetime = LocalDateTime.of(date, time);
    }

    public int getTicketsSold() {
        return ticketsSold;
    }

    public void addTicketAmountOfOrderToTicketsSold(int amount){
        ticketsSold += amount;
    }
}

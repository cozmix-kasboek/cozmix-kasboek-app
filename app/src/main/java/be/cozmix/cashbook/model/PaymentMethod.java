package be.cozmix.cashbook.model;

public class PaymentMethod {

    private int id;
    private String name;

    public PaymentMethod(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}

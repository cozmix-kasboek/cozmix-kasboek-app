package be.cozmix.cashbook.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Objects;

@Entity(tableName = "users")
public class User {

    @PrimaryKey
    private int id;
    @ColumnInfo(name = "first_name")
    private String firstName;
    @ColumnInfo(name = "last_name")
    private String surname;
    @ColumnInfo(name = "img_src")
    private String image;
    @ColumnInfo(name = "role_id")
    private int role;

    @JsonCreator
    public User(
            @JsonProperty("id") int id,
            @JsonProperty("first_name") String firstName,
            @JsonProperty("last_name") String surname,
            @JsonProperty("img_src") String image,
            @JsonProperty("role_id") int role) {
        this.id = id;
        this.firstName = firstName;
        this.surname = surname;
        this.image = image;
        this.role = role;
    }

    @JsonProperty("id")
    public int getId() {
        return id;
    }

    @JsonProperty("first_name")
    public String getFirstName() {
        return firstName;
    }

    @JsonProperty("last_name")
    public String getSurname() {
        return surname;
    }

    @JsonProperty("img_src")
    public String getImage() {
        return image;
    }

    @JsonProperty("role_id")
    public int getRole() {
        return role;
    }

    public String getName() {
        return String.format("%s %s", firstName, surname);
    }

    public boolean isAdmin() {
        return role == 2;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return id == user.id &&
                image.equals(user.image) &&
                role == user.role &&
                firstName.equals(user.firstName) &&
                surname.equals(user.surname);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, firstName, surname, image, role);
    }

}

package be.cozmix.cashbook.model;

import androidx.annotation.NonNull;
import androidx.room.Entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity(primaryKeys = {"transactionId", "productId"})
public class TransactionProductCrossRef {
    @JsonIgnore
    public int transactionId;
    public int productId;
    public int quantity;

    public TransactionProductCrossRef(int transactionId, int productId, int quantity) {
        this.transactionId = transactionId;
        this.productId = productId;
        this.quantity = quantity;
    }

    @Override
    @NonNull
    public String toString() {
        return "TransactionProductCrossRef{" +
                "transactionId=" + transactionId +
                ", productId=" + productId +
                ", quantity=" + quantity + "}";
    }
}

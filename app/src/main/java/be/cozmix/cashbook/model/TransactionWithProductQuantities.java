package be.cozmix.cashbook.model;

import androidx.room.Embedded;

public class TransactionWithProductQuantities {

    @Embedded
    public Transaction transaction;

    public int productId;
    public int quantity;

}

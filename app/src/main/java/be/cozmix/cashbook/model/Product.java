package be.cozmix.cashbook.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonValue;

import java.util.Objects;

@Entity(tableName = "products")
public class Product {

    @PrimaryKey(autoGenerate = true)
    private int productId;
    @ColumnInfo(name = "name")
    private String name;
    @ColumnInfo(name = "price")
    private double price;
    @ColumnInfo(name = "category_id")
    private int category_id;

    @Ignore
    @JsonProperty("description")
    private String description;

    @Ignore
    @JsonProperty("code_id")
    private int code;

    @Ignore
    public Product(int productId, String name, int category_id) {
        this.productId = productId;
        this.name = name;
        this.category_id = category_id;
    }

    public Product(int productId, String name, double price, int category_id) {
        this.productId = productId;
        this.name = name;
        this.price = price;
        this.category_id = category_id;
    }

    @Ignore
    @JsonCreator
    public Product(@JsonProperty("id") int id,
                   @JsonProperty("name") String name,
                   @JsonProperty("price") double price,
                   @JsonProperty("description") String description,
                   @JsonProperty("code_id") int code,
                   @JsonProperty("category_id") int category_id) {
        this.productId = id;
        this.name = name;
        this.price = price;
        this.description = description;
        this.code = code;
        this.category_id = category_id;
    }

    @JsonValue
    public int getProductId() {
        return productId;
    }

    public String getName() {
        return name;
    }

    public double getPrice() {
        return round(price, 2);
    }

    public int getCategory_id() {
        return category_id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Product product = (Product) o;
        return productId == product.productId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(productId);
    }

    public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        long factor = (long) Math.pow(10, places);
        value = value * factor;
        long tmp = Math.round(value);
        return (double) tmp / factor;
    }
}

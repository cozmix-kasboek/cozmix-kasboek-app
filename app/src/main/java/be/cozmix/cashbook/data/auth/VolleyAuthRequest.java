package be.cozmix.cashbook.data.auth;

import android.content.Context;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.fasterxml.jackson.core.JsonProcessingException;

import org.json.JSONException;
import org.json.JSONObject;

import be.cozmix.cashbook.utils.CashBookException;
import be.cozmix.cashbook.utils.RequestObject;
import be.cozmix.cashbook.utils.Callback;

public class VolleyAuthRequest implements Authorization {

    private static final String TAG = VolleyAuthRequest.class.getSimpleName();
    private final String errorMessage = "Something went wrong while sending the user data!";

    private final RequestQueue requestQueue;

    private static VolleyAuthRequest instance;

    private VolleyAuthRequest(Context context) {
        requestQueue = Volley.newRequestQueue(context);
    }

    static VolleyAuthRequest getInstance(Context context) {
        if (instance == null) {
            instance = new VolleyAuthRequest(context);
        }
        return instance;
    }

    @Override
    public void login(String pin, final Callback callback) {
        try {
            RequestObject body = new RequestObject();
            body.add("pin", pin);

            String route = "https://cozmix.lauwaet.be/api/roles/admin/login";
            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, route,
                    body.getJSONObject(), new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        callback.onResponse(response);
                    } catch (JSONException exc) {
                        Log.e(TAG, "onResponse: Something went wrong while reading JSON!", exc);
                        callback.onFailure(new CashBookException(errorMessage, exc));
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e(TAG, "onErrorResponse: Something went wrong " +
                            "while logging in the user!", error);
                    callback.onFailure(new CashBookException(errorMessage, error));
                }
            });

            jsonObjectRequest.setTag(TAG);
            requestQueue.add(jsonObjectRequest);
        } catch (JSONException exc) {
            Log.e(TAG, "login: Something went wrong while creating the JSON body!", exc);
            callback.onFailure(new CashBookException(errorMessage, exc));
        } catch (JsonProcessingException exc) {
            Log.e(TAG, "login: Something went wrong while processing the JSON body!", exc);
            callback.onFailure(new CashBookException(errorMessage, exc));
        }
    }

}

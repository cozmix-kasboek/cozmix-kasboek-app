package be.cozmix.cashbook.data.transactions;

import androidx.lifecycle.LiveData;

import java.time.LocalDate;
import java.util.List;

import be.cozmix.cashbook.data.utils.RoomCallback;
import be.cozmix.cashbook.model.Transaction;
import be.cozmix.cashbook.model.TransactionProductCrossRef;
import be.cozmix.cashbook.model.TransactionWithProductQuantities;
import be.cozmix.cashbook.model.TransactionWithProducts;

public interface ITransactionEntityManager {
    LiveData<List<Transaction>> getAll();
    Transaction getTransactionById(int id);
    LiveData<List<Transaction>> getTransactionsByDate(LocalDate date);
    List<TransactionWithProducts> getTransactionsWithProducts();
    List<TransactionWithProductQuantities> getTransactionsWithProductQuantities();
    void add(Transaction transaction);
    void addAndGetId(Transaction transaction, RoomCallback callback);
    void update(Transaction transaction);
    void delete(Transaction transaction);
    void deleteAll();
    void insertAllTransactionProductCrossReferences(List<TransactionProductCrossRef> transactions);
    void insertTransactionProductCrossReference(TransactionProductCrossRef transactionProductCrossRef);
    List<Transaction> getAllForSynchronization();
    void clean();

}

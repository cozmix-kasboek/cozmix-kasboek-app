package be.cozmix.cashbook.data.products;

import android.content.Context;

import androidx.lifecycle.LiveData;

import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import be.cozmix.cashbook.data.database.LocalRoomDatabase;
import be.cozmix.cashbook.model.Product;

public class ProductEntityManager implements IProductEntityManager{

    private ProductDao mProductDao;
    private LiveData<List<Product>> mProducts;
    private Executor executor  =Executors.newSingleThreadExecutor();

    public ProductEntityManager(Context context) {
        LocalRoomDatabase db = LocalRoomDatabase.getInstance(context);
        mProductDao = db.productDao();
        mProducts = db.productDao().getAll();
    }

    @Override
    public LiveData<List<Product>> getProducts(){
        return mProducts;
    }

    @Override
    public LiveData<List<Product>> getTickets() {
        return mProductDao.getProductsByCategory(1);
    }

    @Override
    public LiveData<List<Product>> getGadgets(){
        return mProductDao.getProductsByCategory(2);
    }

    @Override
    public LiveData<List<Product>> getExpenses() {
        return mProductDao.getProductsByCategory(3);
    }

    @Override
    public LiveData<List<Product>> getDiverse() {
        return mProductDao.getProductsByCategory(4);
    }

    @Override
    public void updateProduct(Product product) {
        executor.execute(new Runnable() {
            @Override
            public void run() {
                mProductDao.update(product);
            }
        });
    }

    @Override
    public void insertProduct(Product product) {
        executor.execute(new Runnable() {
            @Override
            public void run() {
                mProductDao.insert(product);
            }
        });
    }

    @Override
    public void deleteProduct(Product product) {
        executor.execute(new Runnable() {
            @Override
            public void run() {
                mProductDao.delete(product);
            }
        });
    }

    @Override
    public List<Product> getAllProductsForSynchronization() {
        return mProductDao.getAllProductsForSynchronization();
    }

    @Override
    public Product getProductByIdForSynchronization(int id) {
        return mProductDao.getProductByIdForSynchronization(id);
    }


}

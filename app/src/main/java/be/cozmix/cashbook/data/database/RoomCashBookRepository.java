package be.cozmix.cashbook.data.database;

import android.content.Context;

import androidx.lifecycle.LiveData;

import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import be.cozmix.cashbook.data.events.EventEntityManager;
import be.cozmix.cashbook.data.products.ProductEntityManager;
import be.cozmix.cashbook.data.transactions.TransactionEntityManager;
import be.cozmix.cashbook.data.users.UserDao;
import be.cozmix.cashbook.data.users.UserEntityManager;
import be.cozmix.cashbook.model.User;

public class RoomCashBookRepository implements CashBookRepository {
    private static RoomCashBookRepository instance;

    private UserDao mUserDao;
    private LiveData<List<User>> mUsers;
    private Executor executor = Executors.newSingleThreadExecutor();

    private UserEntityManager userEntityManager;
    private EventEntityManager eventEntityManager;
    private ProductEntityManager productEntityManager;
    private TransactionEntityManager transactionEntityManager;

    private RoomCashBookRepository(Context context) {
        LocalRoomDatabase db = LocalRoomDatabase.getInstance(context);
        mUserDao = db.userDao();
        mUsers = mUserDao.getAll();

        userEntityManager = new UserEntityManager(context);
        eventEntityManager = new EventEntityManager(context);
        productEntityManager = new ProductEntityManager(context);
        transactionEntityManager = new TransactionEntityManager(context);
    }

    public static RoomCashBookRepository getInstance(Context context) {
        if (instance == null) {
            instance = new RoomCashBookRepository(context);
        }
        return instance;
    }

    @Override
    public UserEntityManager getUserEntityManager(){
        return userEntityManager;
    }

    @Override
    public EventEntityManager getEventEntityManager() {
        return eventEntityManager;
    }

    @Override
    public ProductEntityManager getProductEntityManager() {
        return productEntityManager;
    }

    @Override
    public TransactionEntityManager getTransactionEntityManager() {
        return transactionEntityManager;
    }

    @Override
    public LiveData<List<User>> getUsers() {
        return mUsers;
    }

    @Override
    public LiveData<User> getUserById(int id) {
        return mUserDao.getUserById(id);
    }

    @Override
    public void addUser(User user) {
        executor.execute(new Runnable() {
            @Override
            public void run() {
                mUserDao.insert(user);
            }
        });
    }

    @Override
    public void insertAllUsers(List<User> users) {
        executor.execute(new Runnable() {
            @Override
            public void run() {
                mUserDao.insertAll(users);
            }
        });
    }

    @Override
    public void updateUser(User user) {
        executor.execute(new Runnable() {
            @Override
            public void run() {
                mUserDao.update(user);
            }
        });
    }

    @Override
    public void deleteUser(User user) {
        executor.execute(new Runnable() {
            @Override
            public void run() {
                mUserDao.delete(user);
            }
        });
    }

    @Override
    public void deleteAllUsers() {
        executor.execute(new Runnable() {
            @Override
            public void run() {
                mUserDao.deleteAll();
            }
        });
    }


}

package be.cozmix.cashbook.data.events;

import android.content.Context;

import androidx.lifecycle.LiveData;

import java.time.LocalDate;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import be.cozmix.cashbook.data.database.LocalRoomDatabase;
import be.cozmix.cashbook.model.Event;

public class EventEntityManager implements IEventEntityManager {

    private EventDao mEventDao;
    private LiveData<List<Event>> mEvents;
    private Executor executor = Executors.newSingleThreadExecutor();


    public EventEntityManager(Context context) {
        LocalRoomDatabase db = LocalRoomDatabase.getInstance(context);
        mEventDao = db.eventDao();
        mEvents = db.eventDao().getAll();
    }
    @Override
    public LiveData<List<Event>> getAllEvents() {
        return mEvents;
    }

    @Override
    public LiveData<Event> getEventById(String id) {
        return mEventDao.getEventById(id);
    }

    @Override
    public LiveData<List<Event>> getEventsByDate(LocalDate date) {
        return mEventDao.getEventsByDate(date.toString());
    }

    @Override
    public Event getEventByIdForTransaction(String id) {

        return null;
    }

    @Override
    public void updateEvent(Event event) {
        executor.execute(new Runnable() {
            @Override
            public void run() {
                mEventDao.update(event);
            }
        });
    }

    @Override
    public void insertEvent(Event event) {
        executor.execute(new Runnable() {
            @Override
            public void run() {
                mEventDao.insert(event);
            }
        });
    }

    @Override
    public void deleteEvent(Event event) {
        executor.execute(new Runnable() {
            @Override
            public void run() {
                mEventDao.delete(event);
            }
        });
    }

    @Override
    public void deleteAllEvents() {
        executor.execute(new Runnable() {
            @Override
            public void run() {
                mEventDao.deleteAll();
            }
        });
    }

    @Override
    public List<Event> getAllForSynchronization() {
        return mEventDao.getAllForSynchronization();
    }

    @Override
    public void clean() {
        executor.execute(new Runnable() {
            @Override
            public void run() {
                mEventDao.clean();
            }
        });
    }
}

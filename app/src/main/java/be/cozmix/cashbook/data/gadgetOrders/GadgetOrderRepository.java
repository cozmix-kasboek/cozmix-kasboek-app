package be.cozmix.cashbook.data.gadgetOrders;

import androidx.lifecycle.LiveData;

import java.util.List;

import be.cozmix.cashbook.model.GadgetOrder;

public interface GadgetOrderRepository {

    LiveData<List<GadgetOrder>> getAll();

    void deleteItem(GadgetOrder gadgetOrder);

    void addItem(int id, String name, double Price);

    void clearAll();
}

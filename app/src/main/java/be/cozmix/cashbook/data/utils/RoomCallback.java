package be.cozmix.cashbook.data.utils;

public interface RoomCallback {

    void onSuccess(int id);

}

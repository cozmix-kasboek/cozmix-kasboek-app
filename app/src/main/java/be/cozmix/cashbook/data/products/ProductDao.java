package be.cozmix.cashbook.data.products;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import be.cozmix.cashbook.model.Product;


@Dao
public interface ProductDao {

    @Query("SELECT * FROM products ORDER BY name ASC")
    LiveData<List<Product>> getAll();

    @Query("SELECT * FROM products WHERE category_id = :id")
    LiveData<List<Product>> getProductsByCategory(int id);

    @Query("DELETE FROM products")
    void deleteAll();

    @Query("SELECT * FROM products WHERE productId = :id")
    Product getProductById(int id);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Product product);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<Product> products);

    @Update
    void update(Product product);

    @Delete
    void delete(Product product);

    @Query("SELECT * FROM products ORDER BY name ASC")
    List<Product> getAllProductsForSynchronization();

    @Query("SELECT * FROM products WHERE productId = :id")
    Product getProductByIdForSynchronization(int id);

}

package be.cozmix.cashbook.data.users;

import android.content.Context;

import androidx.lifecycle.LiveData;

import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import be.cozmix.cashbook.data.database.LocalRoomDatabase;
import be.cozmix.cashbook.model.User;

public class UserEntityManager implements IUserEntityManager {

    private UserDao mUserDao;
    private LiveData<List<User>> mUsers;
    private Executor executor = Executors.newSingleThreadExecutor();


    public UserEntityManager(Context context) {
        LocalRoomDatabase db = LocalRoomDatabase.getInstance(context);
        mUserDao = db.userDao();
        mUsers = db.userDao().getAll();
    }

    @Override
    public LiveData<List<User>> getAllUsers() {
        return mUsers;
    }

    @Override
    public LiveData<User> getUserById(int id) {
        return mUserDao.getUserById(id);
    }

    @Override
    public void updateUser(User user) {
        executor.execute(new Runnable() {
            @Override
            public void run() {
                mUserDao.update(user);
            }
        });
    }

    @Override
    public void insertUser(User user) {
        executor.execute(new Runnable() {
            @Override
            public void run() {
                mUserDao.insert(user);
            }
        });
    }

    @Override
    public void deleteUser(User user) {
        executor.execute(new Runnable() {
            @Override
            public void run() {
                mUserDao.delete(user);
            }
        });
    }

    @Override
    public void deleteAllUsers() {
        executor.execute(new Runnable() {
            @Override
            public void run() {
                mUserDao.deleteAll();
            }
        });
    }

    @Override
    public void insertAllUsers(List<User> users) {
        executor.execute(new Runnable() {
            @Override
            public void run() {
                mUserDao.insertAll(users);
            }
        });
    }

    @Override
    public List<User> getAllUsersForSynchronization() {
        return mUserDao.getAllUsersForSynchronization();
    }

    @Override
    public User getUserByIdForSynchronization(int id) {
        return mUserDao.getUserByIdForSynchronization(id);
    }

}

package be.cozmix.cashbook.data.utils;

import androidx.room.TypeConverter;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class LocalDateTimeConverter {

    private static final DateTimeFormatter formatter =
            DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

    @TypeConverter
    public static LocalDateTime toDateTime(String dateTime) {
        return dateTime == null ? null : LocalDateTime.parse(dateTime, formatter);
    }

    @TypeConverter
    public static String fromDateTime(LocalDateTime dateTime) {
        return dateTime == null ? null : dateTime.format(formatter);
    }

}

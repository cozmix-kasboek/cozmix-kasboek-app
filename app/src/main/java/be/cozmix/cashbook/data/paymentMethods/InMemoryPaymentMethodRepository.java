package be.cozmix.cashbook.data.paymentMethods;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import be.cozmix.cashbook.model.PaymentMethod;

public class InMemoryPaymentMethodRepository implements PaymentMethodRepository{

    private static PaymentMethodRepository instance;
    private List<PaymentMethod> paymentMethods = new ArrayList<>(Arrays.asList(
            new PaymentMethod(1, "Cash"),
            new PaymentMethod(2, "Payconiq"),
            new PaymentMethod(3, "Overschijving")

    ));


    public static PaymentMethodRepository getInstance() {
        if (instance == null) {
            instance = new InMemoryPaymentMethodRepository();
        }
        return instance;
    }

    public int getPaymentIdByName(String name){
       for (PaymentMethod paymentMethod : paymentMethods){
           if (paymentMethod.getName().equals(name)){
               return paymentMethod.getId();
           }
       }
       return 0;
    }



}

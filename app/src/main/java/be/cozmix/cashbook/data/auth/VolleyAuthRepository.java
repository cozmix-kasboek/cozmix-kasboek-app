package be.cozmix.cashbook.data.auth;

import android.content.Context;

import be.cozmix.cashbook.utils.Callback;

public class VolleyAuthRepository implements Authorization {

    private static VolleyAuthRepository instance;

    private final VolleyAuthRequest volleyAuthRequest;

    private VolleyAuthRepository(Context context) {
        this.volleyAuthRequest = VolleyAuthRequest.getInstance(context);
    }

    public static VolleyAuthRepository getInstance(Context context) {
        if (instance == null) {
            instance = new VolleyAuthRepository(context);
        }
        return instance;
    }

    @Override
    public void login(String pin, Callback callback) {
        volleyAuthRequest.login(pin, callback);
    }

}

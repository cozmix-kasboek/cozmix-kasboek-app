package be.cozmix.cashbook.data.synchronization.workers;

import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.concurrent.futures.CallbackToFutureAdapter;
import androidx.work.ListenableWorker;
import androidx.work.WorkerParameters;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.util.concurrent.ListenableFuture;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import be.cozmix.cashbook.data.database.CashBookRepository;
import be.cozmix.cashbook.data.database.RoomCashBookRepository;
import be.cozmix.cashbook.data.synchronization.differences.Synchronization;
import be.cozmix.cashbook.data.synchronization.differences.UserSynchronization;
import be.cozmix.cashbook.model.User;
import be.cozmix.cashbook.utils.Callback;
import be.cozmix.cashbook.utils.CashBookException;

public class UserSynchronizationWorker extends ListenableWorker {

    /*
     * TAG groups related work together.
     * NAME uniquely identifies this worker.
     */
    public static final String TAG = "synchronization";
    public static final String NAME = UserSynchronizationWorker.class.getSimpleName();
    private final String errorMessage = "Something went wrong while fetching the user data!";

    private Executor executor;
    private RequestQueue requestQueue;
    private CashBookRepository cashBookRepository;

    public UserSynchronizationWorker(
            @NonNull Context context,
            @NonNull WorkerParameters workerParams) {
        super(context, workerParams);
        this.executor = Executors.newSingleThreadExecutor();
        this.requestQueue = Volley.newRequestQueue(context);
        this.cashBookRepository = RoomCashBookRepository.getInstance(context);
    }

    @NonNull
    @Override
    public ListenableFuture<Result> startWork() {
        return CallbackToFutureAdapter.getFuture(completer -> {
            Callback callback = new Callback() {
                @Override
                public void onFailure(CashBookException exc) {
                    completer.set(Result.retry());
                }
                @Override
                public void onResponse(JSONObject response) {
                    executor.execute(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                List<User> users = read(response);
                                Synchronization<User> sync = new UserSynchronization(cashBookRepository);
                                sync.synchronize(users);
                                completer.set(Result.success());
                            } catch (IOException | JSONException exc) {
                                completer.set(Result.retry());
                            }
                        }
                    });
                }
            };

            completer.addCancellationListener(new Runnable() {
                @Override
                public void run() {
                    if (requestQueue != null) {
                        requestQueue.cancelAll(TAG);
                    }
                }
            }, executor);

            getUsers(callback);
            return callback;
        });
    }

    private void getUsers(Callback callback) {
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,
            "https://cozmix.lauwaet.be/api/users", null,
            new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        callback.onResponse(response);
                    } catch (JSONException exc) {
                        Log.e(NAME, "onResponse: Something went wrong while reading JSON!", exc);
                        callback.onFailure(new CashBookException(errorMessage, exc));
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    callback.onFailure(new CashBookException(errorMessage, error));
                }
        });
        jsonObjectRequest.setTag(TAG);
        requestQueue.add(jsonObjectRequest);
    }

    private List<User> read(JSONObject response) throws JSONException, IOException {
        String data = response.getJSONArray("data").toString();
        ObjectMapper mapper = new ObjectMapper();
        return Arrays.asList(mapper.readValue(data, User[].class));
    }

}

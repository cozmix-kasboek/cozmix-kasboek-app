package be.cozmix.cashbook.data.synchronization.workers;

import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.concurrent.futures.CallbackToFutureAdapter;
import androidx.work.ListenableWorker;
import androidx.work.WorkerParameters;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.util.concurrent.ListenableFuture;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import be.cozmix.cashbook.data.database.CashBookRepository;
import be.cozmix.cashbook.data.database.RoomCashBookRepository;
import be.cozmix.cashbook.data.messages.TransactionsMessage;
import be.cozmix.cashbook.model.Transaction;
import be.cozmix.cashbook.model.TransactionWithProductQuantities;
import be.cozmix.cashbook.model.TransactionWithProducts;
import be.cozmix.cashbook.utils.Callback;
import be.cozmix.cashbook.utils.CashBookException;

public class TransactionSynchronizationWorker extends ListenableWorker {

    /*
     * TAG groups related work together.
     * NAME uniquely identifies this worker.
     */
    public static final String TAG = "synchronization";
    public static final String NAME = TransactionSynchronizationWorker.class.getSimpleName();
    private final String errorMessage = "Something went wrong while fetching the transaction data!";

    private Executor executor;
    private ObjectMapper mapper;
    private RequestQueue requestQueue;
    private CashBookRepository cashBookRepository;

    public TransactionSynchronizationWorker(
            @NonNull Context context,
            @NonNull WorkerParameters workerParams) {
        super(context, workerParams);
        this.mapper = new ObjectMapper();
        this.executor = Executors.newSingleThreadExecutor();
        this.requestQueue = Volley.newRequestQueue(context);
        this.cashBookRepository = RoomCashBookRepository.getInstance(context);
    }

    @NonNull
    @Override
    public ListenableFuture<Result> startWork() {
        return CallbackToFutureAdapter.getFuture(completer -> {
            Callback callback = new Callback() {
                @Override
                public void onFailure(CashBookException exc) {
                    completer.set(Result.retry());
                }

                @Override
                public void onResponse(JSONObject response) {
                    completer.set(Result.success());
                }
            };

            completer.addCancellationListener(new Runnable() {
                @Override
                public void run() {
                    if (requestQueue != null) {
                        requestQueue.cancelAll(TAG);
                    }
                }
            }, executor);

            postTransactions(callback);
            return callback;
        });
    }

    private void postTransactions(Callback callback) {
        // This must be handled on a separate thread (to avoid UI locking)!
        Executors.newSingleThreadExecutor().execute(new Runnable() {
            @Override
            public void run() {
                // Get the transactions
                List<TransactionWithProductQuantities> transactions =
                        cashBookRepository.getTransactionEntityManager().getTransactionsWithProductQuantities();

                if (transactions.isEmpty()) {
                    try {
                        callback.onResponse(null);
                    } catch (JSONException exc) {
                        Log.e(NAME, "onResponse: Something went wrong while reading JSON!", exc);
                        callback.onFailure(new CashBookException(errorMessage, exc));
                    }
                } else {
                    try {
                        // Create the body
                        TransactionsMessage transactionsMessage = new TransactionsMessage(transactions);
                        String json = mapper.writeValueAsString(transactionsMessage);
                        System.out.println(json);
                        JSONObject body = new JSONObject(json);

                        // Create the request
                        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST,
                                "https://cozmix.lauwaet.be/api/transactions", body,
                                new Response.Listener<JSONObject>() {
                                    @Override
                                    public void onResponse(JSONObject response) {
                                        try {
                                            callback.onResponse(response);
                                        } catch (JSONException exc) {
                                            Log.e(NAME, "onResponse: Something went wrong while reading JSON!", exc);
                                            callback.onFailure(new CashBookException(errorMessage, exc));
                                        }
                                    }
                                }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                callback.onFailure(new CashBookException(errorMessage, error));
                            }
                        });

                        // Add the request to the request queue
                        jsonObjectRequest.setTag(TAG);
                        requestQueue.add(jsonObjectRequest);
                    } catch (JSONException | JsonProcessingException exc) {
                        Log.e(NAME, "onResponse: Something went wrong while building the body!", exc);
                        callback.onFailure(new CashBookException(errorMessage, exc));
                    }
                }
            }
        });
    }

}

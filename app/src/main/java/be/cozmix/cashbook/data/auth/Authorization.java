package be.cozmix.cashbook.data.auth;

import be.cozmix.cashbook.utils.Callback;

public interface Authorization {

    void login(String pin, Callback callback);

}

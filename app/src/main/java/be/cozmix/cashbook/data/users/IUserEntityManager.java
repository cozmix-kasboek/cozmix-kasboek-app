package be.cozmix.cashbook.data.users;

import androidx.lifecycle.LiveData;

import java.util.List;

import be.cozmix.cashbook.model.User;

public interface IUserEntityManager {

    LiveData<List<User>> getAllUsers();
    LiveData<User> getUserById(int id);
    void updateUser(User user);
    void insertUser(User user);
    void deleteUser(User user);
    void deleteAllUsers();
    void insertAllUsers(List<User> users);
    List<User> getAllUsersForSynchronization();
    User getUserByIdForSynchronization(int id);

}

package be.cozmix.cashbook.data.synchronization.differences;

import androidx.annotation.NonNull;

import java.util.List;

public interface Synchronization<T> {

    void synchronize(@NonNull List<T> response);
    boolean isInList(int id, List<T> list);

}

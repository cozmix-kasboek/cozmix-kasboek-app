package be.cozmix.cashbook.data.synchronization.differences;

import androidx.annotation.NonNull;

import java.util.List;

import be.cozmix.cashbook.data.database.CashBookRepository;
import be.cozmix.cashbook.data.products.IProductEntityManager;
import be.cozmix.cashbook.model.Product;

public class ProductSynchronization implements Synchronization<Product> {

    private final IProductEntityManager productManager;

    public ProductSynchronization(@NonNull CashBookRepository repository) {
        this.productManager = repository.getProductEntityManager();
    }

    @Override
    public void synchronize(@NonNull List<Product> response) {
        for (Product current : response) {
            Product local = productManager.getProductByIdForSynchronization(current.getProductId());
            if (local != null) {
                if (!current.equals(local)) {
                    productManager.updateProduct(current);
                }
            } else {
                productManager.insertProduct(current);
            }
        }

        List<Product> database = productManager.getAllProductsForSynchronization();
        if (database != null) {
            for (Product current: database) {
                if (!isInList(current.getProductId(), response)) {
                    productManager.deleteProduct(current);
                }
            }
        }
    }

    @Override
    public boolean isInList(int id, List<Product> list) {
        for (Product current : list) {
            if (id == current.getProductId()) {
                return true;
            }
        }

        return false;
    }

}

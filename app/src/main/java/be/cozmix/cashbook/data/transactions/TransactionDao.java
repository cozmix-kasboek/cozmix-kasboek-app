package be.cozmix.cashbook.data.transactions;


import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Transaction;
import androidx.room.Update;

import java.util.List;

import be.cozmix.cashbook.model.Product;
import be.cozmix.cashbook.model.TransactionProductCrossRef;
import be.cozmix.cashbook.model.TransactionWithProductQuantities;
import be.cozmix.cashbook.model.TransactionWithProducts;

@Dao
public interface TransactionDao {

    @Transaction
    @Query("SELECT * FROM transactions")
    LiveData<List<be.cozmix.cashbook.model.Transaction>> getAllTransactions();

    @Query("SELECT * from transactions WHERE transactionId = :id")
    be.cozmix.cashbook.model.Transaction getTransactionById(int id);

    @Query("SELECT * FROM transactions WHERE date(date) = :date")
    LiveData<List<be.cozmix.cashbook.model.Transaction>> getTransactionsByDate(String date);

    @Query("SELECT t.*, tp.productId, tp.quantity " +
            "FROM transactions t " +
            "JOIN TransactionProductCrossRef tp ON t.transactionId == tp.transactionId")
    List<TransactionWithProductQuantities> getTransactionsWithProductQuantities();

    @Transaction
    @Query("SELECT * FROM transactions")
    List<TransactionWithProducts> getTransactionsWithProducts();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAllTransactions(List<be.cozmix.cashbook.model.Transaction> transactions);

    @Query("DELETE FROM transactions")
    void deleteAllTransactions();


    /* Products */
    @Query("SELECT * FROM products")
    List<Product> getAllProducts();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAllProducts(List<Product> products);

    @Query("DELETE FROM products")
    void deleteAllProducts();

    /* Transaction Product Cross Reference */
    @Query("SELECT * FROM TransactionProductCrossRef")
    List<TransactionProductCrossRef> getAllTransactionProductCrossRefs();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAllTransactionProductCrossReferences(List<TransactionProductCrossRef> transactionProductCrossRefs);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertTransactionProductCrossReference(TransactionProductCrossRef transactionProductCrossRef);

    @Query("DELETE FROM TransactionProductCrossRef")
    void deleteAllTransactionProductCrossReferences();


    @Insert
    long insert(be.cozmix.cashbook.model.Transaction transaction);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<be.cozmix.cashbook.model.Transaction> transactions);

    @Update
    void update(be.cozmix.cashbook.model.Transaction transaction);

    @Delete
    void delete(be.cozmix.cashbook.model.Transaction transaction);

    @Query("DELETE FROM transactions")
    void deleteAll();

    @Query("SELECT * FROM transactions")
    List<be.cozmix.cashbook.model.Transaction> getAllForSynchronization();

    @Query("DELETE FROM transactions WHERE date < date('now', '-7 day')")
    void clean();

}

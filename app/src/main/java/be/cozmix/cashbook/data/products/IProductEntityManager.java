package be.cozmix.cashbook.data.products;

import androidx.lifecycle.LiveData;

import java.util.List;

import be.cozmix.cashbook.model.Product;

public interface IProductEntityManager {

    LiveData<List<Product>> getProducts();
    LiveData<List<Product>> getTickets();
    LiveData<List<Product>> getGadgets();
    LiveData<List<Product>> getExpenses();
    LiveData<List<Product>> getDiverse();
    void updateProduct(Product product);
    void insertProduct(Product product);
    void deleteProduct(Product product);
    List<Product> getAllProductsForSynchronization();
    Product getProductByIdForSynchronization(int id);

}

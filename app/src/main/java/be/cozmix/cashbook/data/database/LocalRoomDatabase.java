package be.cozmix.cashbook.data.database;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import be.cozmix.cashbook.data.events.EventDao;
import be.cozmix.cashbook.data.products.ProductDao;
import be.cozmix.cashbook.data.users.UserDao;
import be.cozmix.cashbook.data.transactions.TransactionDao;
import be.cozmix.cashbook.model.Event;
import be.cozmix.cashbook.model.Product;
import be.cozmix.cashbook.model.Transaction;
import be.cozmix.cashbook.model.TransactionProductCrossRef;
import be.cozmix.cashbook.model.User;

@Database(entities = {User.class, Product.class, Transaction.class, TransactionProductCrossRef.class, Event.class}, version = 1, exportSchema = false)
public abstract class LocalRoomDatabase extends RoomDatabase {

    public abstract TransactionDao transactionDao();
    public abstract UserDao userDao();
    public abstract EventDao eventDao();
    public abstract ProductDao productDao();

    private static final String DATABASE_NAME = "transactions_database";
    private static LocalRoomDatabase INSTANCE;

    public static LocalRoomDatabase getInstance(final Context context) {
        if (INSTANCE == null) {
            synchronized (LocalRoomDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            LocalRoomDatabase.class, DATABASE_NAME)
                            .fallbackToDestructiveMigration()
                            .build();
                }
            }
        }
        return INSTANCE;
    }

}

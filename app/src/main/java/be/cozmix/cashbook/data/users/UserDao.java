package be.cozmix.cashbook.data.users;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import be.cozmix.cashbook.model.User;

@Dao
public interface UserDao {
    @Query("SELECT * FROM users ORDER BY first_name ASC")
    LiveData<List<User>> getAll();

    @Query("SELECT * FROM users WHERE id = :id")
    LiveData<User> getUserById(int id);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(User user);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<User> users);

    @Update
    void update(User user);

    @Delete
    void delete(User user);

    @Query("DELETE FROM users")
    void deleteAll();

    @Query("SELECT * FROM users ORDER BY first_name ASC")
    List<User> getAllUsersForSynchronization();

    @Query("SELECT * FROM users WHERE id = :id")
    User getUserByIdForSynchronization(int id);
}

package be.cozmix.cashbook.data.transactions;

import android.content.Context;

import androidx.lifecycle.LiveData;

import java.time.LocalDate;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import be.cozmix.cashbook.data.database.LocalRoomDatabase;
import be.cozmix.cashbook.data.utils.RoomCallback;
import be.cozmix.cashbook.model.Transaction;
import be.cozmix.cashbook.model.TransactionProductCrossRef;
import be.cozmix.cashbook.model.TransactionWithProductQuantities;
import be.cozmix.cashbook.model.TransactionWithProducts;

public class TransactionEntityManager implements ITransactionEntityManager {

    private TransactionDao mTransactionDao;
    private LiveData<List<Transaction>> mTransactions;
    private Executor executor = Executors.newSingleThreadExecutor();

    public TransactionEntityManager(Context context) {
        LocalRoomDatabase db = LocalRoomDatabase.getInstance(context);
        mTransactionDao = db.transactionDao();
        mTransactions = db.transactionDao().getAllTransactions();
    }

    @Override
    public LiveData<List<Transaction>> getAll() {
        return mTransactions;
    }


    @Override
    public Transaction getTransactionById(int id) {
        return mTransactionDao.getTransactionById(id);
    }

    @Override
    public LiveData<List<Transaction>> getTransactionsByDate(LocalDate date) {
        return mTransactionDao.getTransactionsByDate(date.toString());
    }

    @Override
    public List<TransactionWithProducts> getTransactionsWithProducts() {
        return mTransactionDao.getTransactionsWithProducts();
    }

    @Override
    public List<TransactionWithProductQuantities> getTransactionsWithProductQuantities() {
        return mTransactionDao.getTransactionsWithProductQuantities();
    }

    @Override
    public void add(Transaction transaction) {
        executor.execute(new Runnable() {
            @Override
            public void run() {
                mTransactionDao.insert(transaction);
            }
        });
    }

    @Override
    public void addAndGetId(Transaction transaction, RoomCallback callback) {
        executor.execute(new Runnable() {
            @Override
            public void run() {
                long id = mTransactionDao.insert(transaction);
                callback.onSuccess(Math.toIntExact(id));
            }
        });
    }

    @Override
    public void update(Transaction transaction) {
        executor.execute(new Runnable() {
            @Override
            public void run() {
                mTransactionDao.update(transaction);
            }
        });
    }

    @Override
    public void delete(Transaction transaction) {
        executor.execute(new Runnable() {
            @Override
            public void run() {
                mTransactionDao.delete(transaction);
            }
        });
    }

    @Override
    public void deleteAll() {
        executor.execute(new Runnable() {
            @Override
            public void run() {
                mTransactionDao.deleteAll();
            }
        });
    }

    @Override
    public void insertAllTransactionProductCrossReferences(List<TransactionProductCrossRef> transactions) {
        executor.execute(new Runnable() {
            @Override
            public void run() {
                mTransactionDao.insertAllTransactionProductCrossReferences(transactions);
            }
        });
    }

    @Override
    public void insertTransactionProductCrossReference(TransactionProductCrossRef transactionProductCrossRef) {
        executor.execute(new Runnable() {
            @Override
            public void run() {
                mTransactionDao.insertTransactionProductCrossReference(transactionProductCrossRef);
            }
        });
    }

    @Override
    public List<Transaction> getAllForSynchronization() {
        return mTransactionDao.getAllForSynchronization();
    }

    @Override
    public void clean() {
        executor.execute(new Runnable() {
            @Override
            public void run() {
                mTransactionDao.clean();
            }
        });
    }
}

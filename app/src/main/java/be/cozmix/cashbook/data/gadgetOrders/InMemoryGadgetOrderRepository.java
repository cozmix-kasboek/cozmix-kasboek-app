package be.cozmix.cashbook.data.gadgetOrders;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.LinkedList;
import java.util.List;

import be.cozmix.cashbook.model.GadgetOrder;

public class InMemoryGadgetOrderRepository implements GadgetOrderRepository{

    private final MutableLiveData<List<GadgetOrder>> gadgetOrders = new MutableLiveData<>();
    private final MutableLiveData<Double> gadgetOrdersPrice = new MutableLiveData<>();
    private static InMemoryGadgetOrderRepository instance;
    private List<GadgetOrder> list = new LinkedList<>();


    public InMemoryGadgetOrderRepository() {
        gadgetOrders.setValue(list);
    }

    public static InMemoryGadgetOrderRepository getInstance() {
        if (instance == null) {
            instance = new InMemoryGadgetOrderRepository();
        }
        return instance;
    }

    @Override
    public LiveData<List<GadgetOrder>> getAll() {
        return gadgetOrders;
    }

    @Override
    public void deleteItem(GadgetOrder gadgetOrder) {
        list.remove(gadgetOrder);
        gadgetOrders.setValue(list);
    }

    @Override
    public void addItem(int id, String name, double price) {
        for (GadgetOrder gadgetOrder : list){
            if (gadgetOrder.getProductId() == id){
                gadgetOrder.increaseQuantity();
                gadgetOrders.setValue(list);
                return;
            }
        }
        list.add(new GadgetOrder(id, name, price));
        gadgetOrders.setValue(list);
    }

    @Override
    public void clearAll() {
        list.clear();
        gadgetOrders.setValue(list);
    }


    public double calculateTotalPrice() {
        double total = 0;
        for (GadgetOrder gadgetOrder : list){
            total += gadgetOrder.getPrice() * gadgetOrder.getQuantity();
        }
        return total;
    }

    public List<GadgetOrder> getGadgetOrders(){
        return list;
    }

}

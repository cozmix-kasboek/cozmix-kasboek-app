package be.cozmix.cashbook.data.messages;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

import be.cozmix.cashbook.model.Event;

public class EventsMessage {

    private List<Event> events;

    @JsonCreator
    public EventsMessage(@JsonProperty List<Event> events) {
        this.events = events;
    }

    public List<Event> getEvents() {
        return events;
    }

}

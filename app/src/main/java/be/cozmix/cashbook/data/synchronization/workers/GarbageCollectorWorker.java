package be.cozmix.cashbook.data.synchronization.workers;

import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import be.cozmix.cashbook.data.database.CashBookRepository;
import be.cozmix.cashbook.data.database.RoomCashBookRepository;
import be.cozmix.cashbook.data.events.IEventEntityManager;
import be.cozmix.cashbook.data.transactions.ITransactionEntityManager;

public class GarbageCollectorWorker extends Worker {

    /*
     * TAG groups related work together.
     * NAME uniquely identifies this worker.
     */
    public static final String TAG = "cleanup";
    public static final String NAME = GarbageCollectorWorker.class.getSimpleName();
    private final String message = "Cleaning up local the events and transactions databases!";

    private IEventEntityManager eventManager;
    private ITransactionEntityManager transactionManager;

    public GarbageCollectorWorker(
            @NonNull Context context,
            @NonNull WorkerParameters workerParams) {
        super(context, workerParams);
        CashBookRepository cashBookRepository = RoomCashBookRepository.getInstance(context);
        this.eventManager = cashBookRepository.getEventEntityManager();
        this.transactionManager = cashBookRepository.getTransactionEntityManager();
    }

    @NonNull
    @Override
    public Result doWork() {
        Log.i(TAG, String.format("doWork: %s", message));
        eventManager.clean();
        transactionManager.clean();
        return Result.success();
    }

}

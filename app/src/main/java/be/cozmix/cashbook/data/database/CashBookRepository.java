package be.cozmix.cashbook.data.database;

import androidx.lifecycle.LiveData;

import java.util.List;

import be.cozmix.cashbook.data.events.EventEntityManager;
import be.cozmix.cashbook.data.products.ProductEntityManager;
import be.cozmix.cashbook.data.transactions.TransactionEntityManager;
import be.cozmix.cashbook.data.users.UserEntityManager;
import be.cozmix.cashbook.model.User;

public interface CashBookRepository {

    LiveData<List<User>> getUsers();
    LiveData<User> getUserById(int id);
    void addUser(User user);
    void insertAllUsers(List<User> users);
    void updateUser(User user);
    void deleteUser(User user);
    void deleteAllUsers();
    UserEntityManager getUserEntityManager();
    EventEntityManager getEventEntityManager();
    ProductEntityManager getProductEntityManager();
    TransactionEntityManager getTransactionEntityManager();

}

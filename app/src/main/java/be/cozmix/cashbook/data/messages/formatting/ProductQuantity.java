package be.cozmix.cashbook.data.messages.formatting;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

class ProductQuantity {

    private int productId;
    private int quantity;

    @JsonCreator
    public ProductQuantity(
            @JsonProperty int productId,
            @JsonProperty int quantity) {
        this.productId = productId;
        this.quantity = quantity;
    }

    public int getProductId() {
        return productId;
    }

    public int getQuantity() {
        return quantity;
    }

}

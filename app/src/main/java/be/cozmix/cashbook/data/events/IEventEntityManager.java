package be.cozmix.cashbook.data.events;

import androidx.lifecycle.LiveData;

import java.time.LocalDate;
import java.util.List;

import be.cozmix.cashbook.model.Event;

public interface IEventEntityManager {

    LiveData<List<Event>> getAllEvents();
    LiveData<Event> getEventById(String id);
    LiveData<List<Event>> getEventsByDate(LocalDate date);
    Event getEventByIdForTransaction(String id);
    void updateEvent(Event event);
    void insertEvent(Event event);
    void deleteEvent(Event event);
    void deleteAllEvents();
    List<Event> getAllForSynchronization();
    void clean();

}

package be.cozmix.cashbook.data.ticketOrders;

import androidx.lifecycle.LiveData;

import java.util.List;

import be.cozmix.cashbook.model.TicketOrder;

public interface TicketOrderRepository {

    LiveData<List<TicketOrder>> getAll();
    void deleteItem(TicketOrder ticketOrder);
    void addItem(int id, String name, double Price);
    void clearAll();
    double calculateTotalPrice();
    int getTicketAmount();
}

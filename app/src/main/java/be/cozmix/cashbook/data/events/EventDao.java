package be.cozmix.cashbook.data.events;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import be.cozmix.cashbook.model.Event;

@Dao
public interface EventDao {
    @Query("SELECT * FROM events ORDER BY datetime ASC")
    LiveData<List<Event>> getAll();

    @Query("SELECT * FROM events WHERE id = :id")
    LiveData<Event> getEventById(String id);

    @Query("SELECT * FROM events WHERE date(datetime) = :date")
    LiveData<List<Event>> getEventsByDate(String date);

    @Insert
    void insert(Event event);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<Event> events);

    @Update
    void update(Event event);

    @Delete
    void delete(Event event);

    @Query("DELETE FROM events")
    void deleteAll();

    @Query("SELECT * FROM events ORDER BY datetime ASC")
    List<Event> getAllForSynchronization();

    @Query("DELETE FROM events WHERE datetime < date('now', '-7 day')")
    void clean();
}

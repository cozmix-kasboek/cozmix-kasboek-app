package be.cozmix.cashbook.data.messages;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

import be.cozmix.cashbook.data.messages.formatting.TransactionFormatted;
import be.cozmix.cashbook.data.messages.formatting.TransactionsFormatter;
import be.cozmix.cashbook.model.TransactionWithProductQuantities;

public class TransactionsMessage {

    private List<TransactionFormatted> transactions;

    @JsonCreator
    public TransactionsMessage(@JsonProperty List<TransactionWithProductQuantities> transactions) {
        TransactionsFormatter formatter = new TransactionsFormatter(transactions);
        this.transactions = formatter.format();
    }

    public List<TransactionFormatted> getTransactions() {
        return transactions;
    }

}

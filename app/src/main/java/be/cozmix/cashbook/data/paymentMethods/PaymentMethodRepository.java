package be.cozmix.cashbook.data.paymentMethods;

public interface PaymentMethodRepository {
    int getPaymentIdByName(String name);
}

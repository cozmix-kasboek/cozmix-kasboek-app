package be.cozmix.cashbook.data;

import android.app.Application;

import be.cozmix.cashbook.data.auth.Authorization;
import be.cozmix.cashbook.data.auth.VolleyAuthRepository;
import be.cozmix.cashbook.data.database.CashBookRepository;
import be.cozmix.cashbook.data.database.RoomCashBookRepository;

public class Repositories {

    public static Authorization getAuthorization(Application application) {
        return VolleyAuthRepository.getInstance(application.getApplicationContext());
    }

    public static CashBookRepository getCashBookRepository(Application application) {
        return RoomCashBookRepository.getInstance(application);
    }


}

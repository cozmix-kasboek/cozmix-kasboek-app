package be.cozmix.cashbook.data.ticketOrders;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.LinkedList;
import java.util.List;

import be.cozmix.cashbook.model.TicketOrder;

public class InMemoryTicketOrderRepository implements TicketOrderRepository {


    private final MutableLiveData<List<TicketOrder>> ticketOrders = new MutableLiveData<>();
    private static InMemoryTicketOrderRepository instance;
    private List<TicketOrder> list = new LinkedList<>();

    public InMemoryTicketOrderRepository() {
        ticketOrders.setValue(list);
    }

    public static InMemoryTicketOrderRepository getInstance() {
        if (instance == null){
            instance = new InMemoryTicketOrderRepository();
        }
        return instance;
    }

    @Override
    public LiveData<List<TicketOrder>> getAll() {
        return ticketOrders;
    }

    @Override
    public void deleteItem(TicketOrder ticketOrder) {
        list.remove(ticketOrder);
        ticketOrders.setValue(list);
    }

    @Override
    public void addItem(int id, String name, double price) {
        for (TicketOrder ticketOrder : list){
            if (ticketOrder.getProductId() == id) {
                ticketOrder.increaseQuantity();
                ticketOrders.setValue(list);
                return;
            }
        }
        list.add(new TicketOrder(id, name, price));
        ticketOrders.setValue(list);
    }

    @Override
    public double calculateTotalPrice() {
        double total = 0;
        for (TicketOrder ticketOrder : list){
            total += ticketOrder.getPrice() * ticketOrder.getQuantity();
        }
        return total;
    }

    @Override
    public int getTicketAmount() {
        int ticketAmount = 0;
        for (TicketOrder ticketOrder : list){
            ticketAmount += ticketOrder.getQuantity();
        }
        return ticketAmount;
    }


    @Override
    public void clearAll() {
        list.clear();
        ticketOrders.setValue(list);

    }

    public List<TicketOrder> getTicketOrders(){
        return list;
    }
}

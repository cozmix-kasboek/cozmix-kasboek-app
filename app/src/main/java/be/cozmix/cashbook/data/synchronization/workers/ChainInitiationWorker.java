package be.cozmix.cashbook.data.synchronization.workers;

import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.work.BackoffPolicy;
import androidx.work.Constraints;
import androidx.work.NetworkType;
import androidx.work.OneTimeWorkRequest;
import androidx.work.PeriodicWorkRequest;
import androidx.work.WorkManager;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import java.util.concurrent.TimeUnit;

// This chain initiation worker provides control over the sequence of the other workers!
public class ChainInitiationWorker extends Worker {

    private final Context context;

    // Constraints
    private final Constraints constraints = new Constraints.Builder()
            .setRequiredNetworkType(NetworkType.CONNECTED)
            .setRequiresBatteryNotLow(true)
            .build();

    // Work Requests
    private OneTimeWorkRequest synchronizeUsersWorkRequest;
    private OneTimeWorkRequest synchronizeEventsWorkRequest;
    private OneTimeWorkRequest synchronizeProductsWorkRequest;
    private OneTimeWorkRequest synchronizeTransactionsWorkRequest;
    private OneTimeWorkRequest garbageCollectorWorkRequest;

    /*
     * TAG groups related work together.
     * NAME uniquely identifies this worker.
     */
    public static final String TAG = "chain-initiation";
    public static final String NAME = ChainInitiationWorker.class.getSimpleName();
    private final String message = "Initiating worker chain!";

    public ChainInitiationWorker(
            @NonNull Context context,
            @NonNull WorkerParameters workerParams) {
        super(context, workerParams);
        this.context = context;
        createWorkRequests();
    }

    @NonNull
    @Override
    public Result doWork() {
        Log.i(TAG, String.format("doWork: %s", message));
        WorkManager.getInstance(context)
                .beginWith(synchronizeUsersWorkRequest)
                .then(synchronizeProductsWorkRequest)
                .then(synchronizeEventsWorkRequest)
                .then(synchronizeTransactionsWorkRequest)
                .then(garbageCollectorWorkRequest)
                .enqueue();
        return Result.success();
    }

    private void createWorkRequests() {
        synchronizeUsersWorkRequest = new OneTimeWorkRequest
                .Builder(UserSynchronizationWorker.class)
                .setConstraints(constraints)
                .setBackoffCriteria(
                        BackoffPolicy.LINEAR,
                        PeriodicWorkRequest.MIN_BACKOFF_MILLIS,
                        TimeUnit.MILLISECONDS
                )
                .build();

        synchronizeEventsWorkRequest = new OneTimeWorkRequest
                .Builder(EventSynchronizationWorker.class)
                .setConstraints(constraints)
                .setBackoffCriteria(
                        BackoffPolicy.LINEAR,
                        PeriodicWorkRequest.MIN_BACKOFF_MILLIS,
                        TimeUnit.MILLISECONDS
                )
                .build();

        synchronizeProductsWorkRequest = new OneTimeWorkRequest
                .Builder(ProductSynchronizationWorker.class)
                .setConstraints(constraints)
                .setBackoffCriteria(
                        BackoffPolicy.LINEAR,
                        PeriodicWorkRequest.MIN_BACKOFF_MILLIS,
                        TimeUnit.MILLISECONDS
                )
                .build();

        synchronizeTransactionsWorkRequest = new OneTimeWorkRequest
                .Builder(TransactionSynchronizationWorker.class)
                .setConstraints(constraints)
                .setBackoffCriteria(
                        BackoffPolicy.LINEAR,
                        PeriodicWorkRequest.MIN_BACKOFF_MILLIS,
                        TimeUnit.MILLISECONDS
                )
                .build();

        garbageCollectorWorkRequest = new OneTimeWorkRequest
                .Builder(GarbageCollectorWorker.class)
                .setConstraints(constraints)
                .setBackoffCriteria(
                        BackoffPolicy.LINEAR,
                        PeriodicWorkRequest.MIN_BACKOFF_MILLIS,
                        TimeUnit.MILLISECONDS
                )
                .build();
    }

}

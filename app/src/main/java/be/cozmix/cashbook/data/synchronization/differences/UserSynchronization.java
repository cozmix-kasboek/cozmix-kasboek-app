package be.cozmix.cashbook.data.synchronization.differences;

import androidx.annotation.NonNull;

import java.util.List;

import be.cozmix.cashbook.data.database.CashBookRepository;
import be.cozmix.cashbook.data.users.IUserEntityManager;
import be.cozmix.cashbook.model.User;

public class UserSynchronization implements Synchronization<User> {

    private final IUserEntityManager userManager;

    public UserSynchronization(@NonNull CashBookRepository repository) {
        this.userManager = repository.getUserEntityManager();
    }

    @Override
    public void synchronize(@NonNull List<User> response) {
        for (User current : response) {
            User local = userManager.getUserByIdForSynchronization(current.getId());
            if (local != null) {
                if (!current.equals(local)) {
                    userManager.updateUser(current);
                }
            } else {
                userManager.insertUser(current);
            }
        }

        List<User> database = userManager.getAllUsersForSynchronization();
        if (database != null) {
            for (User current : database) {
                if (!isInList(current.getId(), response)) {
                    userManager.deleteUser(current);
                }
            }
        }
    }

    @Override
    public boolean isInList(int id, List<User> list) {
        for (User current : list) {
            if (id == current.getId()) {
                return true;
            }
        }

        return false;
    }

}

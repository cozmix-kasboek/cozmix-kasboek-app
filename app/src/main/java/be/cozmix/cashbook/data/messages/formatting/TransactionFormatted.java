package be.cozmix.cashbook.data.messages.formatting;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;

import be.cozmix.cashbook.model.Transaction;
import be.cozmix.cashbook.model.TransactionWithProductQuantities;

public class TransactionFormatted {

    private Transaction transaction;
    private List<ProductQuantity> products;

    @JsonCreator
    public TransactionFormatted(
            @JsonProperty TransactionWithProductQuantities twpq) {
        this.products = new ArrayList<>();
        this.transaction = twpq.transaction;
        this.addProductQuantity(twpq.productId, twpq.quantity);
    }

    public void addProductQuantity(int product, int quantity) {
        this.products.add(new ProductQuantity(product, quantity));
    }

    public Transaction getTransaction() {
        return transaction;
    }

    public List<ProductQuantity> getProducts() {
        return products;
    }

}

package be.cozmix.cashbook.data.messages.formatting;

import java.util.ArrayList;
import java.util.List;

import be.cozmix.cashbook.model.TransactionWithProductQuantities;

public class TransactionsFormatter {

    private List<TransactionWithProductQuantities> input;

    public TransactionsFormatter(List<TransactionWithProductQuantities> transactions) {
        this.input = transactions;
    }

    public List<TransactionFormatted> format() {
        List<TransactionFormatted> result = new ArrayList<>();

        if (input != null && input.size() > 0) {
            int id = 0;
            TransactionFormatted current = new TransactionFormatted(input.get(id));
            for (TransactionWithProductQuantities twpq : input) {
                if (twpq.transaction.getTransactionId() != id) {
                    // new TransactionsFormatted and add to list
                    current = new TransactionFormatted(twpq);
                    result.add(current);
                    id++;
                } else {
                    // send product and quantity to current
                    current.addProductQuantity(twpq.productId, twpq.quantity);
                }
            }
        }

        return result;
    }

}

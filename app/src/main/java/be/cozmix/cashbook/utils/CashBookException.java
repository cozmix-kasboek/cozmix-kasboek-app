package be.cozmix.cashbook.utils;

public class CashBookException extends RuntimeException {

    public CashBookException() {
        super();
    }

    public CashBookException(String msg) {
        super(msg);
    }

    public CashBookException(String msg, Throwable t) {
        super(msg, t);
    }

}

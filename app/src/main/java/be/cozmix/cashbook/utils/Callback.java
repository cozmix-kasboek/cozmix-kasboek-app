package be.cozmix.cashbook.utils;

import org.json.JSONException;
import org.json.JSONObject;

public interface Callback {

    void onFailure(CashBookException exc);
    void onResponse(JSONObject response) throws JSONException;

}

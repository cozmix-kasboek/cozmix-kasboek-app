package be.cozmix.cashbook.utils;

import android.util.SparseArray;

public class TransactionTypeMapper {

    private final SparseArray<String> myMap = new SparseArray<>();

    public TransactionTypeMapper() {
        myMap.put(1, "Ticketverkoop" );
        myMap.put(2, "Gadgetverkoop");
        myMap.put(3, "Kas-bank");
        myMap.put(4, "Divers");
        myMap.put(5, "Lidgeld");
        myMap.put(6, "Onkosten");
    }

    public SparseArray<String> getMyMap() {
        return myMap;
    }
}

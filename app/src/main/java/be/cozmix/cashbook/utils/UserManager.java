package be.cozmix.cashbook.utils;

import be.cozmix.cashbook.model.User;

public class UserManager {

    private static UserManager instance;

    private User user;

    // Save a bearer token for logout (and other API requests to the Laravel server).
    private String token;

    private UserManager() {
        // Empty private constructor for singleton pattern (limits access).
    }

    public static UserManager getInstance() {
        if(instance == null) {
            instance = new UserManager();
        }
        return instance;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public boolean isLoggedIn() {
        return user != null;
    }

    public boolean isLoggedOut() {
        return !isLoggedIn();
    }

    public boolean hasToken() {
        return token.trim().length() > 0;
    }

    public void logout() {
        user = null;
        token = null;
    }

}

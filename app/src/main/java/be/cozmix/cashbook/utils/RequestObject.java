package be.cozmix.cashbook.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class RequestObject {

    private Map<String, String> body;

    public RequestObject() {
        body = new HashMap<>();
    }

    public void add(String key, String value) {
        body.put(key, value);
    }

    public JSONObject getJSONObject() throws JsonProcessingException, JSONException {
        ObjectMapper mapper = new ObjectMapper();
        String json = mapper.writeValueAsString(body);
        return new JSONObject(json);
    }
    
}

package be.cozmix.cashbook.utils;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import be.cozmix.cashbook.ui.viewmodels.EventEditViewModel;
import be.cozmix.cashbook.ui.viewmodels.LoginViewModel;

public class ViewModelFactory extends ViewModelProvider.NewInstanceFactory {

    private Application mApplication;
    private int mParameter;
    private String mParameterString;

    public ViewModelFactory(Application application, int parameter) {
        this.mApplication = application;
        this.mParameter = parameter;
        this.mParameterString = "";
    }

    public ViewModelFactory(Application application, String paramater) {
        this.mApplication = application;
        this.mParameter = 0;
        this.mParameterString = paramater;
    }

    @Override
    @NonNull
    public <T extends ViewModel> T create(Class<T> modelClass) {
        if(modelClass.isAssignableFrom(LoginViewModel.class)) {
            return (T) new LoginViewModel(mApplication, mParameter);
        } else if (modelClass.isAssignableFrom(EventEditViewModel.class)) {
            return (T) new EventEditViewModel(mApplication, mParameterString);
        } else {
            throw new IllegalArgumentException("Unknown ViewModel class");
        }
    }

}

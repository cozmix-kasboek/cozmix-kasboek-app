package be.cozmix.cashbook.utils;

import be.cozmix.cashbook.model.Event;

public class EventHolder {
    private Event event;
    public Event getEvent(){
        return event;
    }

    public void setEvent(Event event) {
        this.event = event;
    }

    private static final EventHolder holder = new EventHolder();
    public static EventHolder getInstance(){
        return holder;
    }
}

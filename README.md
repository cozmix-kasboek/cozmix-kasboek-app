# Cozmix kasboek mobiele applicatie

Een mobiele applicatie om transacties op te slaan en automatisch te synchroniseren met de back-end zodat die ook op afstand via een webbrowser geraadpleegd kunnen worden.

## Set-up

0. (Indien gewenst: een API-key genereren (zie back-endproject) en in de header van de volley requests zetten)
1. APK installeren op tablet of project clonen en via Android Studio draaien op een via USB aangesloten tablet.

## Gebruik

*De app is gemaakt om in landscape mode te worden gebruikt.*

Gewone gebruikers hoeven geen PIN-code in te geven, administrators hebben als standaard code **1234**.

## Functionaliteiten

### Gebruikers
Alle gebruikers die via het beheerplatform ingesteld zijn, worden bij het opstarten van de applicatie in een overzicht weergegeven. Hierbij dient men zijn eigen gebruiker te kiezen en de identiteit te bevestigen. In het geval van een gewone gebruiker, bevestigt men simpelweg of de naam van de gebruiker correct is. Een administrator daarentegen vereist een PIN code om in te loggen. Deze is standaard als 1234 ingesteld in het beheerplatform. Zodra men ingelogd is, komt de gebruiker op het hoofdscherm van waaruit allerlei opties mogelijk zijn. Deze functionaliteiten worden hieronder verder beschreven. 

### Afmelden
Nadat men ingelogd is als een bepaalde gebruiker, kan men vanaf het hoofdscherm terug afmelden (om dan als nieuwe gebruiker aan te melden). Wanneer men op de knop afmelden drukt, komt de gebruiker terug op het oorspronkelijke startscherm met het overzicht van de gebruikers. 

### Gebeurtenissen
De gebruiker kan onmiddellijk gebeurtenissen toevoegen. Hiervoor vult men een formulier in, waar men de naam van de gebeurtenis alsook de datum en tijd meegeeft. Zodra deze gebeurtenis opgeslagen werd, kun je deze in een overzicht bekijken. Vanuit dit overzicht bestaat ook de mogelijkheid om de gebeurtenissen te wijzigen of te verwijderen. 

### Ticketverkoop
Eenmaal een gebeurtenis toegevoegd is, wordt die in het hoofdscherm weergegeven als een knop. Via die knop kan men dan ticketverkoop registreren. Zo kan men soorten tickets selecteren en toevoegen om de verkoop vast te leggen. Zodra de tickets geselecteerd zijn met hun juiste aantallen, komt men in de betaalcyclus terecht. Van hieruit kan je de transactie opslaan als “betaald via cash” of kan de Payconiq app geopend worden (indien deze lokaal op de tablet geïnstallleerd is). 

### Gadgetverkoop
Vanuit het hoofdscherm kan de gebruiker ook gadgets verkopen. Dit betreft dan de fysieke speeltjes of gadgets die vanuit de shop verkocht worden. Eerst komt men op een scherm dat een overzicht van alle gadgets weergeeft. Op dat scherm selecteer je een gadget om te verkopen. In een volgende stap kom je in een winkelmandje terecht, waar alle gadgets en hun hoeveelheid bijgehouden worden. Hier wordt dan ook de totaalprijs weergegeven. Eenmaal het winkelmandje bevestigd wordt, komt men in de betaalcyclus terecht. Van hieruit kan je de transactie opslaan als “betaald via cash” of kan de Payconiq app geopend worden (indien deze lokaal op de tablet geïnstallleerd is). 

### Andere transacties
Er is ook een knop om alle overige transacties te beheren. Wanneer men op deze knop drukt, komt men op een scherm met meerdere opties: lidgeld, onkostennota, diverse, van kas naar bank en omgekeerd. Deze transacties kunnen van hieruit geregistreerd worden in het kasboek. 

### Transactiebeheer
Alle transacties van de voorbije week kan men bekijken in een overzicht. Hier bestaat de optie om een transactie volledig ongedaan te maken en uit het systeem te wissen (een grote “undo” operatie). 


## Aandachtspunten

### Validatie
Bij de formulieren voor invoegen van een PIN code of bij het toevoegen of wijzigen van gebeurtenissen vindt er live validatie plaats. Bij deze validatie hebben we zelf enkele regels vastgelegd die eventueel aangepast kunnen worden naar wens. Hiervoor kan men de “validate()” methode aanpassen binnen de concrete validatie klasses onder de ui>utils package. 

### Synchronisatie
Voor de concrete synchronisatie van server naar applicatie en omgekeerd maken we gebruik van de WorkManager API van Android. Hiervoor leggen we de sequentiële volgorde van de synchronisatie vast in de “doWork()” methode van de ChainInitiationWorker klasse onder de data>synchronization>workers package. Indien gewenst kan men hier de volgorde aanpassen. Houd de GarbageCollectorWorker altijd als laatste, aangezien die alle gebeurtenissen en transacties ouder dan 7 dagen verwijdert uit de lokale databank. Net omdat die worker als laatste aan de beurt is, zal die geen data verwijderen die nog niet gesynchroniseerd is: als de transacties nog niet naar de server gesycnhroniseerd zijn, dan zal de GarbageCollectorWorker ook geen data mogen verwijderen van de WorkManager. Houd dit dus in het achterhoofd wanneer men wijzigingen aan de synchronisatei doorvoert. 


## Verdere ontwikkeling

### Tonen van foto's bij gebruikers en gadgets
Behoorde initieel tot de scope van het project, maar werd niet uitgewerkt.

### Retroactief zaken kunnen wijziging in de app
Bijvoorbeeld een transactie registreren die gisteren of de dag ervoor niet of verkeerd werd geregistreerd.